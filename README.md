# Advent of Code 2023

This is my participation to Advent of Code 2023, written in C#.

I mainly wanted to learn and practice C#, so some parts might not be idiomatic.

To run and get all the answers:

```
$ dotnet run --project AdventOfCode2023
```

If you want only some days, say the 9th and the 12th:

```
$ dotnet run --project AdventOfCode2023 -- 9 12
```

You can also check the solutions against the examples given by Advent of Code with:

```
$ dotnet test AdventOfCode2023Tests
```
