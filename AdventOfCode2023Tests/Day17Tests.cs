namespace AdventOfCode2023Tests;

public class Day17Tests
{
    [Fact]
    public void Day17Answers()
    {
        var input = @"2413432311323
3215453535623
3255245654254
3446585845452
4546657867536
1438598798454
4457876987766
3637877979653
4654967986887
4564679986453
1224686865563
2546548887735
4322674655533";

        var (a1, a2) = new Day17().Solve(input);
        Assert.Equal(102L, a1);
        Assert.Equal(94L, a2);
    }
}
