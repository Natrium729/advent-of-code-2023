namespace AdventOfCode2023Tests;

public class Day14Tests
{
    [Fact]
    public void Day14Answers()
    {
        var input = @"O....#....
O.OO#....#
.....##...
OO.#O....O
.O.....O#.
O.#..O.#.#
..O..#O..O
.......O..
#....###..
#OO..#....";

        var (a1, a2) = new Day14().Solve(input);
        Assert.Equal(136L, a1);
        Assert.Equal(64L, a2);
    }
}
