namespace AdventOfCode2023Tests;

public class Day21Tests
{
    [Fact]
    public void Day21Answers()
    {
        var input = @"...........
.....###.#.
.###.##..#.
..#.#...#..
....#.#....
.##..S####.
.##..#...#.
.......##..
.##.#.####.
.##..##.##.
...........";

        var (a1, _) = new Day21(6).Solve(input);
        Assert.Equal(16L, a1);
    }
}
