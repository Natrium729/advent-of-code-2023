namespace AdventOfCode2023Tests;

public class Day6Tests
{
    [Fact]
    public void Day6Answers()
    {
        var input = @"Time:      7  15   30
Distance:  9  40  200
";

        var (a1, a2) = new Day6().Solve(input);
        Assert.Equal(288L, a1);
        Assert.Equal(71503L, a2);
    }
}
