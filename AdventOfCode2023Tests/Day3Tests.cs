namespace AdventOfCode2023Tests;

public class Day3Tests
{
    [Fact]
    public void Day3Answers()
    {
        var input = @"467..114..
...*......
..35..633.
......#...
617*......
.....+.58.
..592.....
......755.
...$.*....
.664.598..";

        var (a1, a2) = new Day3().Solve(input);
        Assert.Equal(4361, a1);
        Assert.Equal(467835, a2);
    }
}
