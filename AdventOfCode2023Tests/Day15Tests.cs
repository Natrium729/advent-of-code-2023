namespace AdventOfCode2023Tests;

public class Day15Tests
{
    [Fact]
    public void Day15Answers()
    {
        var input = @"rn=1,cm-,qp=3,cm=2,qp-,pc=4,ot=9,ab=5,pc-,pc=6,ot=7";

        var (a1, a2) = new Day15().Solve(input);
        Assert.Equal(1320L, a1);
        Assert.Equal(145L, a2);
    }
}
