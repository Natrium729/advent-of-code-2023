namespace AdventOfCode2023Tests;

public class Day12Tests
{
    [Fact]
    public void Day12Answers()
    {
        // Each input will consist of only 1 line.
        var inputs = @"???.### 1,1,3
.??..??...?##. 1,1,3
?###???????? 3,2,1";
        (long, long)[] answers = { (1, 1), (4, 16384), (10, 506250) };

        foreach (var (input, (expected1, expected2)) in Utils.SplitLines(inputs).Zip(answers))
        {
            var (a1, a2) = new Day12().Solve(input);
            Assert.Equal(expected1, a1);
            Assert.Equal(expected2, a2);
        }
    }
}
