namespace AdventOfCode2023Tests;

public class Day13Tests
{
    [Fact]
    public void Day13Answers()
    {
        var input = @"#.##..##.
..#.##.#.
##......#
##......#
..#.##.#.
..##..##.
#.#.##.#.

#...##..#
#....#..#
..##..###
#####.##.
#####.##.
..##..###
#....#..#";

        var (a1, a2) = new Day13().Solve(input);
        Assert.Equal(405L, a1);
        Assert.Equal(400L, a2);
    }
}
