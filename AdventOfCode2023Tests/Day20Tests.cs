namespace AdventOfCode2023Tests;

public class Day20Tests
{
    [Fact]
    public void Day20Answers()
    {
        var input = @"broadcaster -> a, b, c
%a -> b
%b -> c
%c -> inv
&inv -> a";

        var (a1, _) = new Day20().Solve(input);
        Assert.Equal(32000000L, a1);

        var input2 = @"broadcaster -> a
%a -> inv, con
&inv -> b
%b -> con
&con -> output";

        var (aa1, _) = new Day20().Solve(input2);
        Assert.Equal(11687500L, aa1);
    }
}
