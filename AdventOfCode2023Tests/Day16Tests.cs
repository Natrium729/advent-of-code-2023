namespace AdventOfCode2023Tests;

public class Day16Tests
{
    [Fact]
    public void Day16Answers()
    {
        var input = @".|...\....
|.-.\.....
.....|-...
........|.
..........
.........\
..../.\\..
.-.-/..|..
.|....-|.\
..//.|....";

        var (a1, a2) = new Day16().Solve(input);
        Assert.Equal(46L, a1);
        Assert.Equal(51L, a2);
    }
}
