namespace AdventOfCode2023Tests;

public class Day9Tests
{
    [Fact]
    public void Day9Answers()
    {
        var input = @"0 3 6 9 12 15
1 3 6 10 15 21
10 13 16 21 30 45";

        var (a1, a2) = new Day9().Solve(input);
        Assert.Equal(114L, a1);
        Assert.Equal(2L, a2);
    }
}
