namespace AdventOfCode2023Tests;

public class Day8Tests
{
    [Fact]
    public void Day8Answers()
    {
        var input = @"RL

AAA = (BBB, CCC)
BBB = (DDD, EEE)
CCC = (ZZZ, GGG)
DDD = (DDD, DDD)
EEE = (EEE, EEE)
GGG = (GGG, GGG)
ZZZ = (ZZZ, ZZZ)";

        var (a1, _) = new Day8().Solve(input);
        Assert.Equal(2, a1);

        var input2 = @"LLR

AAA = (BBB, BBB)
BBB = (AAA, ZZZ)
ZZZ = (ZZZ, ZZZ)";

        var (aa1, _) = new Day8().Solve(input2);
        Assert.Equal(6, aa1);

        var input3 = @"LR

11A = (11B, XXX)
11B = (XXX, 11Z)
11Z = (11B, XXX)
22A = (22B, XXX)
22B = (22C, 22C)
22C = (22Z, 22Z)
22Z = (22B, 22B)
XXX = (XXX, XXX)";

        var (_, aaa2) = new Day8 { SkipPart1 = true }.Solve(input3);
        Assert.Equal(6L, aaa2);
    }
}
