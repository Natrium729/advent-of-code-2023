namespace AdventOfCode2023Tests;

public class Day22Tests
{
    [Fact]
    public void Day22Answers()
    {
        var input = @"1,0,1~1,2,1
0,0,2~2,0,2
0,2,3~2,2,3
0,0,4~0,2,4
2,0,5~2,2,5
0,1,6~2,1,6
1,1,8~1,1,9";

        var (a1, a2) = new Day22().Solve(input);
        Assert.Equal(5L, a1);
        Assert.Equal(7L, a2);
    }
}
