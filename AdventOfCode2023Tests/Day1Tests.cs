namespace AdventOfCode2023Tests;

public class Day1Tests
{
    [Fact]
    public void Day1Answers()
    {
        var input = "1abc2\npqr3stu8vwx\na1b2c3d4e5f\ntreb7uchet";
        var (a1, _) = new Day1().Solve(input);
        Assert.Equal(142, a1);

        var input2 = "two1nine\neightwothree\nabcone2threexyz\nxtwone3four\n4nineeightseven2\nzoneight234\n7pqrstsixteen";
        var (_, a2) = new Day1().Solve(input2);
        Assert.Equal(281, a2);
    }
}
