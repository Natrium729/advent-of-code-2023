namespace AdventOfCode2023Tests;

public class Day7Tests
{
    [Fact]
    public void Day7Answers()
    {
        var input = @"32T3K 765
T55J5 684
KK677 28
KTJJT 220
QQQJA 483";

        var (a1, a2) = new Day7().Solve(input);
        Assert.Equal(6440, a1);
        Assert.Equal(5905, a2);
    }
}
