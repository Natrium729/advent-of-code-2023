namespace AdventOfCode2023Tests;

public class Day10Tests
{
    [Fact]
    public void Day10Answers()
    {
        var input = @"-L|F7
7S-7|
L|7||
-L-J|
L|-JF";

        var (a1, _) = new Day10().Solve(input);
        Assert.Equal(4L, a1);

        var input2 = @"7-F7-
.FJ|7
SJLL7
|F--J
LJ.LJ";

        var (aa1, _) = new Day10().Solve(input2);
        Assert.Equal(8L, aa1);

        var input3 = @"...........
.S-------7.
.|F-----7|.
.||.....||.
.||.....||.
.|L-7.F-J|.
.|..|.|..|.
.L--J.L--J.
...........";

        var (_, a2) = new Day10().Solve(input3);
        Assert.Equal(4L, a2);

        var input4 = @"..........
.S------7.
.|F----7|.
.||....||.
.||....||.
.|L-7F-J|.
.|..||..|.
.L--JL--J.
..........";

        var (_, aa2) = new Day10().Solve(input4);
        Assert.Equal(4L, aa2);

        var input5 = @".F----7F7F7F7F-7....
.|F--7||||||||FJ....
.||.FJ||||||||L7....
FJL7L7LJLJ||LJ.L-7..
L--J.L7...LJS7F-7L7.
....F-J..F7FJ|L7L7L7
....L7.F7||L7|.L7L7|
.....|FJLJ|FJ|F7|.LJ
....FJL-7.||.||||...
....L---J.LJ.LJLJ...";

        var (_, aaa2) = new Day10().Solve(input5);
        Assert.Equal(8L, aaa2);

        var input6 = @"FF7FSF7F7F7F7F7F---7
L|LJ||||||||||||F--J
FL-7LJLJ||||||LJL-77
F--JF--7||LJLJ7F7FJ-
L---JF-JLJ.||-FJLJJ7
|F|F-JF---7F7-L7L|7|
|FFJF7L7F-JF7|JL---7
7-L-JL7||F7|L7F-7F7|
L.L7LFJ|||||FJL7||LJ
L7JLJL-JLJLJL--JLJ.L";

        var (_, aaaa2) = new Day10().Solve(input6);
        Assert.Equal(10L, aaaa2);
    }
}
