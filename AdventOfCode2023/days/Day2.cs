namespace Natrium729.AdventOfCode2023;

public class Day2 : IDay
{
    public (object, object) Solve(string input)
    {
        var games = ParseInput(input);
        return (Answer1(games), Answer2(games));
    }

    class Set
    {
        public int Red { get; init; }
        public int Green { get; init; }
        public int Blue { get; init; }
    }

    class Game
    {
        public int Id { get; }
        public Set[] Sets { get; }

        public Game(int id, Set[] sets)
        {
            Id = id;
            Sets = sets;
        }
    }

    static Game[] ParseInput(string input)
    {
        return (
            from line in input.Split("\n")
            let trimmedLine = line.Trim()
            where trimmedLine != ""
            select ParseInputLine(trimmedLine)
        ).ToArray();
    }

    static Game ParseInputLine(string line)
    {
        var split = line.Split(": ");
        var idChars = split[0].Trim().SkipWhile(ch => ch < '0' || ch > '9');
        var id = int.Parse(string.Concat(idChars));

        var sets = (
            from part in split[1].Split("; ")
            select ParseSet(part.Trim())
        ).ToArray();

        return new Game(id, sets);
    }

    static Set ParseSet(string input)
    {
        int red = 0;
        int green = 0;
        int blue = 0;

        foreach (var part in input.Split(", "))
        {
            var arr = part.Split();
            switch (arr[1])
            {
                case "red": red = int.Parse(arr[0]); break;
                case "green": green = int.Parse(arr[0]); break;
                case "blue": blue = int.Parse(arr[0]); break;
                default: throw new Exception("Invalid color.");
            }
        }

        return new Set { Red = red, Green = green, Blue = blue };
    }

    static int Answer1(Game[] games)
    {
        return (
            from game in games
            where game.Sets.All(set => set.Red <= 12 && set.Green <= 13 && set.Blue <= 14)
            select game.Id
        ).Sum();
    }

    static int Answer2(Game[] games)
    {
        return (
            from game in games
            let minimum = game.Sets.Aggregate((max, current) => new Set
            {
                Red = Math.Max(max.Red, current.Red),
                Green = Math.Max(max.Green, current.Green),
                Blue = Math.Max(max.Blue, current.Blue),
            })
            select minimum.Red * minimum.Green * minimum.Blue
        ).Sum();
    }
}
