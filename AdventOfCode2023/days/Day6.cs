namespace Natrium729.AdventOfCode2023;

public class Day6 : IDay
{
    class Race
    {
        public long Time { get; init; }
        public long Record { get; init; }

        public long DistanceWithTimePressed(long pressed)
        {
            if (pressed > Time) { throw new ArgumentOutOfRangeException(nameof(pressed)); }

            var remaining = Time - pressed;
            return pressed * remaining;
        }
    }
    public (object, object) Solve(string input)
    {
        return (Answer1(input), Answer2(input));
    }


    static long Answer1(string input)
    {
        var lines =
            from line in input.Split("\n", StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries)
            select
                line.Split(" ", StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries).Skip(1);

        var races = (
            from tuple in lines.First().Zip(lines.ElementAt(1))
            select new Race { Time = int.Parse(tuple.First), Record = int.Parse(tuple.Second) }
        ).ToArray();

        return (
            from race in races
            select (
                from time in Utils.RangeLong(0, race.Time + 1)
                let travelled = race.DistanceWithTimePressed(time)
                where travelled > race.Record
                select 1
            ).Count()
        ).Aggregate((acc, x) => acc * x);
    }

    static long Answer2(string input)
    {
        var lines =
            from line in input.Split("\n", StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries)
            select
                String.Concat(line.Split(" ", StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries).Skip(1));

        var race = new Race { Time = long.Parse(lines.First()), Record = long.Parse(lines.ElementAt(1)) };

        // Bruteforcing it takes a few seconds,
        // but it's not too bad either.
        //
        // We could start from the middle point since the best times are in the middle (I believe)
        // or we could just solve the equation or something.
        return Utils.RangeLong(0, race.Time + 1)
            .Where(t => race.DistanceWithTimePressed(t) > race.Record)
            .Count();
    }
}
