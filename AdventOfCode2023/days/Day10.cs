using System.Diagnostics;

namespace Natrium729.AdventOfCode2023;

public class Day10 : IDay
{
    class Grid
    {
        public string[] Cells { get; }
        public int StartX { get; }
        public int StartY { get; }
        public char StartShape { get; }

        public Grid(string input)
        {
            Cells = Utils.SplitLines(input);
            for (int y = 0; y < Cells.Length; y++)
            {
                for (int x = 0; x < Cells[y].Length; x++)
                {
                    if (Cells[y][x] == 'S')
                    {
                        StartX = x;
                        StartY = y;
                        break;
                    }
                }
            }

            bool northExit = false;
            bool eastExit = false;
            bool southExit = false;
            bool westExit = false;

            if (StartY > 0)
            {
                var above = Cells[StartY - 1][StartX];

                if (above == '|' || above == '7' || above == 'F')
                {
                    northExit = true;
                }
            }

            if (StartY < Cells.Length - 1)
            {
                var below = Cells[StartY + 1][StartX];

                if (below == '|' || below == 'L' || below == 'J')
                {
                    southExit = true;
                }
            }

            if (StartX > 0)
            {
                var left = Cells[StartY][StartX - 1];

                if (left == '-' || left == 'L' || left == 'F')
                {
                    westExit = true;
                }
            }

            if (StartX < Cells[StartY].Length - 1)
            {
                var right = Cells[StartY][StartX + 1];

                if (right == '-' || right == 'J' || right == '7')
                {
                    eastExit = true;
                }
            }

            StartShape = (northExit, eastExit, southExit, westExit) switch
            {
                (true, true, false, false) => 'L',
                (true, false, true, false) => '|',
                (true, false, false, true) => 'J',
                (false, true, true, false) => 'F',
                (false, true, false, true) => '-',
                (false, false, true, true) => '7',
                _ => throw new UnreachableException()
            };
        }

        (Direction, int, int) NextStep(Direction comingFrom, int x, int y)
        {
            var cell = Cells[y][x];
            if (x == StartX && y == StartY)
            {
                cell = StartShape;
            }

            switch (comingFrom)
            {
                case Direction.North:
                    return cell switch
                    {
                        '|' => (Direction.North, x, y + 1),
                        'L' => (Direction.West, x + 1, y),
                        'J' => (Direction.East, x - 1, y),
                        _ => throw new UnreachableException()
                    };
                case Direction.East:
                    return cell switch
                    {
                        '-' => (Direction.East, x - 1, y),
                        'L' => (Direction.South, x, y - 1),
                        'F' => (Direction.North, x, y + 1),
                        _ => throw new UnreachableException()
                    };
                case Direction.South:
                    return cell switch
                    {
                        '|' => (Direction.South, x, y - 1),
                        'F' => (Direction.West, x + 1, y),
                        '7' => (Direction.East, x - 1, y),
                        _ => throw new UnreachableException()
                    };
                case Direction.West:
                    return cell switch
                    {
                        '-' => (Direction.West, x + 1, y),
                        '7' => (Direction.North, x, y + 1),
                        'J' => (Direction.South, x, y - 1),
                        _ => throw new UnreachableException()
                    };
                default: throw new UnreachableException();
            }
        }

        public IEnumerable<(int, int)> GoThrough()
        {
            yield return (StartX, StartY);

            var x = StartX;
            var y = StartY;
            var comingFrom = Direction.North;

            // For the start, choose an arbitrary direction.
            if (StartShape == '-' || StartShape == 'F' || StartShape == 'L')
            {
                comingFrom = Direction.East;
                (comingFrom, x, y) = NextStep(comingFrom, x, y);
                yield return (x, y);
            }
            else if (StartShape == 'J' || StartShape == '7')
            {
                comingFrom = Direction.West;
                (comingFrom, x, y) = NextStep(comingFrom, x, y);
                yield return (x, y);
            }
            else if (StartShape == '|')
            {
                comingFrom = Direction.North;
                (comingFrom, x, y) = NextStep(comingFrom, x, y);
                yield return (x, y);
            }

            while (x != StartX || y != StartY)
            {
                (comingFrom, x, y) = NextStep(comingFrom, x, y);
                yield return (x, y);
            }
        }
    }

    enum Direction
    {
        North,
        East,
        South,
        West,
    }

    public (object, object) Solve(string input)
    {
        var grid = new Grid(input);

        return (Answer1(grid), Answer2(grid));
    }

    static long Answer1(Grid grid)
    {
        return grid.GoThrough().Count() / 2;
    }

    static long Answer2(Grid grid)
    {
    
        // 1. Collect the positions of cells from the pipe loop.

        var pipeCells = new HashSet<(int, int)>();
        foreach (var (x, y) in grid.GoThrough())
        {
            pipeCells.Add((x, y));
        }

        // 2. Cleanup + "unsqueeze" the grid.
        //
        // We replace the junk pipes with '.'.
        // We add blank lines/columns after each line/column
        // So that there's a real path to each exterior cells.

        var height = grid.Cells.Length;
        var processedGrid = new char[height * 2][];

        for (int y = 0; y < height; y++)
        {
            var width = grid.Cells[y].Length;
            processedGrid[y * 2] = new char[width * 2];
            // Add the blank line.
            processedGrid[y * 2 + 1] = Enumerable.Repeat(' ', width * 2).ToArray();

            for (int x = 0; x < width; x++)
            {
                if (pipeCells.Contains((x, y)))
                {
                    // It's part of the loop.
                    processedGrid[y * 2][x * 2] = grid.Cells[y][x];
                }
                else
                {
                    // It isn't part of the loop.
                    processedGrid[y * 2][x * 2] = '.';
                }

                // Add the blank column on that line.
                processedGrid[y * 2][x * 2 + 1] = ' ';

            }
        }

        // 3. Patching the loop.
        //
        // Since we added blank lines/columns,
        // we need to add pipes on theses blank lines/columns
        // so that the pipe loop is unbroken.

        // First we replace the start with its real shape.
        processedGrid[grid.StartY * 2][grid.StartX * 2] = grid.StartShape;

        // Then we can patch the blanks.
        for (int y = 0; y < height; y++)
        {
            var width = grid.Cells[y].Length;

            for (int x = 0; x < width; x++)
            {
                var above = processedGrid[y * 2][x * 2];
                if (above == '|' || above == 'F' || above == '7')
                {
                    processedGrid[y * 2 + 1][x * 2] = '|';
                }

                var left = processedGrid[y * 2][x * 2];
                if (left == '-' || left == 'F' || left == 'L')
                {
                    processedGrid[y * 2][x * 2 + 1] = '-';
                }
            }
        }

        // 4. We flood-fill the blanks and the dots with 'O'.

        // We add the four corners as starts
        // (we assume they are outside of the loop
        // If they weren't, we would need to add blank lines/columns
        // at top and left of the grid, too.).
        var pending = new Stack<(int, int)>();
        pending.Push((0, 0));
        pending.Push((0, processedGrid.Length - 1));
        pending.Push((processedGrid[0].Length - 1, 0));
        pending.Push((processedGrid[0].Length - 1, processedGrid.Length - 1));

        // The flood fill.
        // I think it will visit some cells multiple times
        // (when we push them to the stack while it contains them already)
        // but it's not that bad.
        while (pending.TryPop(out (int, int) next))
        {
            var (x, y) = next;
            processedGrid[y][x] = 'O';

            if (y > 0)
            {
                var above = processedGrid[y - 1][x];
                if (above == '.' || above == ' ')
                {
                    pending.Push((x, y - 1));
                }
            }
            if (y < processedGrid.Length - 1)
            {
                var below = processedGrid[y + 1][x];
                if (below == '.' || below == ' ')
                {
                    pending.Push((x, y + 1));
                }
            }
            if (x > 0)
            {
                var left = processedGrid[y][x - 1];
                if (left == '.' || left == ' ')
                {
                    pending.Push((x - 1, y));
                }
            }
            if (x < processedGrid[0].Length - 1)
            {
                var right = processedGrid[y][x + 1];
                if (right == '.' || right == ' ')
                {
                    pending.Push((x + 1, y));
                }
            }
        }

        // Uncomment to write the resulting grid to a file.
        // using (StreamWriter outputFile = new StreamWriter("day10.txt"))
        // {
        //     foreach (var line in cleanedUpGrid)
        //     {
        //         outputFile.WriteLine(string.Concat(line));
        //     }
        // }

        // Uncomment to print the resulting grid.
        // foreach (var line in processedGrid)
        // {
        //     Console.WriteLine(string.Concat(line));
        // }

        // Since we cleaned up the junk pipes
        // and only added blank cells in the unsqueezing phase,
        // we can simply count the dots that remains for the answer. 
        return processedGrid.Aggregate(0, (acc, line) => acc + line.Count(ch => ch == '.'));
    }
}
