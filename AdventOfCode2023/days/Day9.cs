namespace Natrium729.AdventOfCode2023;

public class Day9 : IDay
{
    public (object, object) Solve(string input)
    {
        var histories = (
            from line in Utils.SplitLines(input)
            select (
                from word in Utils.SplitWords(line)
                select int.Parse(word)
            ).ToArray()
        ).ToArray();

        return (Answer1(histories), Answer2(histories));
    }

    static int[] getEvolution(int[] history)
    {
        var evolution = new int[history.Length - 1];

        for (int i = 0; i < history.Length - 1; i++)
        {
            evolution[i] = history[i + 1] - history[i];
        }

        return evolution;
    }

    static long Answer1(int[][] histories)
    {
        var sum = 0;

        foreach (var history in histories)
        {
            var evolutions = new List<int[]>() { history };

            while (evolutions.Last().Any(x => x != 0))
            {
                evolutions.Add(getEvolution(evolutions.Last()));
            }

            var lastDiff = 0;
            for (int i = evolutions.Count - 2; i >= 0 ; i--)
            {
                lastDiff = evolutions[i].Last() + lastDiff; 
            }

            sum += lastDiff;
        }

        return sum;
    }

    static long Answer2(int[][] histories)
    {
        // That's just the same thing as Answer 1
        // except for a minor change indicated below.

        var sum = 0;

        foreach (var history in histories)
        {
            var evolutions = new List<int[]>() { history };

            while (evolutions.Last().Any(x => x != 0))
            {
                evolutions.Add(getEvolution(evolutions.Last()));
            }

            var lastDiff = 0;
            for (int i = evolutions.Count - 2; i >= 0 ; i--)
            {
                // Here, we take the first instead of the last,
                // and do a subtraction instead of an addition.
                lastDiff = evolutions[i].First() - lastDiff; 
            }

            sum += lastDiff;
        }

        return sum;
    }
}
