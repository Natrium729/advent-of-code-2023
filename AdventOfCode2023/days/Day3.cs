namespace Natrium729.AdventOfCode2023;

public class Day3 : IDay
{
    public (object, object) Solve(string input)
    {
        var schematic = new Schematic(input);
        return (Answer1(schematic), Answer2(schematic));
    }

    static bool IsDigit(char ch)
    {
        return '0' <= ch && ch <= '9';
    }

    class Schematic
    {
        readonly char[][] Grid;

        public Schematic(string input)
        {
            Grid = (
                from string line in input.Split("\n")
                let trimmedLine = line.Trim()
                where trimmedLine != ""
                let charArray = (
                    from char ch in trimmedLine
                        // I tried having `ch == '.' ? null : ch`,
                        // but it said "type inference failed"
                        // and I couldn't solve it.
                        // The explanation of the error didn't explain anything.
                    select ch
                ).ToArray()
                select charArray
            ).ToArray();
        }

        public IEnumerable<(int, int, int)> Numbers()
        {
            for (int y = 0; y < Grid.Length; y++)
            {
                for (int x = 0; x < Grid[y].Length; x++)
                {
                    var cell = Grid[y][x];
                    if (!IsDigit(cell))
                    {
                        continue;
                    }

                    var start = x;
                    do
                    {
                        x++;
                    } while (x < Grid[y].Length && IsDigit(Grid[y][x]));

                    var num = int.Parse(string.Concat(Grid[y].Skip(start).Take(x - start)));
                    yield return (start, y, num);
                }
            }
        }

        public IEnumerable<(int, int, char)> Neighbours(int x, int y)
        {
            for (int dy = -1; dy <= 1; dy++)
            {
                for (int dx = -1; dx <= 1; dx++)
                {
                    var yy = y + dy;
                    var xx = x + dx;
                    var cell = Grid[y][x];
                    if (
                        yy < 0 || yy >= Grid.Length
                        || xx < 0 || xx >= Grid[yy].Length
                    )
                    {
                        continue;
                    }

                    // We'll visit the current cell itself,
                    // but it's OK
                    // because we'll only call that function with digit cells
                    // while looking for symbol cells.
                    yield return (xx, yy, Grid[yy][xx]);
                }
            }
        }
    }

    static int Answer1(Schematic schematic)
    {
        var sum = 0;
        foreach (var (x, y, num) in schematic.Numbers())
        {
            for (int xx = x; xx < x + Math.Floor(Math.Log10(num)) + 1; xx++)
            {
                if (schematic.Neighbours(xx, y).Any(cell => cell.Item3 != '.' && !IsDigit(cell.Item3)))
                {
                    sum += num;
                    break;
                }
            }
        }

        return sum;
    }

    static int Answer2(Schematic schematic)
    {
        // Contains the number neighbours of each star.
        // Maps (xStar, yStar) to HashSet<(xNumber, yNumber, Number)>.
        var stars = new Dictionary<(int, int), HashSet<(int, int, int)>>();
        foreach (var (xNum, yNum, num) in schematic.Numbers())
        {
            for (int xDigit = xNum; xDigit < xNum + Math.Floor(Math.Log10(num)) + 1; xDigit++)
            {
                foreach (var (xStar, yStar, _) in schematic.Neighbours(xDigit, yNum).Where(ch => ch.Item3 == '*'))
                {
                    // We'll visit a star several times but it's OK
                    // because we're using a set.
                    if (!stars.ContainsKey((xStar, yStar)))
                    {
                        stars.Add((xStar, yStar), new HashSet<(int, int, int)>());
                    }
                    stars[(xStar, yStar)].Add((xNum, yNum, num));
                }
            }
        }

        return (
            from star in stars
            where star.Value.Count >= 2
            select star.Value.Aggregate(1, (acc, current) => acc * current.Item3)
        ).Sum();
    }
}
