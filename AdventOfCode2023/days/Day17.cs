using System.Diagnostics;

namespace Natrium729.AdventOfCode2023;

public class Day17 : IDay
{
    class Map
    {
        public int[][] Blocks { get; }

        public Map(string input)
        {
            Blocks = (
                from line in Utils.SplitLines(input)
                select (
                    from ch in line
                    select ch - '0'
                ).ToArray()
            ).ToArray();
        }

        IEnumerable<Node> AvailableNodes(Node node, bool ultra)
        {
            if (ultra && node.Stretch < 4)
            {
                var dest = node.Go(node.Dir);
                if (0 <= dest.Line && dest.Line < Blocks.Length && 0 <= dest.Col && dest.Col < Blocks[0].Length)
                {
                    yield return dest;
                }
                yield break;
            }

            var maxStretch = ultra ? 10 : 3;

            // Can move up.
            if (node.Line > 0 && node.Dir != Direction.Down && !(node.Dir == Direction.Up && node.Stretch >= maxStretch))
            {
                yield return node.Go(Direction.Up);
            }

            // Can move down.
            if (node.Line < Blocks.Length - 1 && node.Dir != Direction.Up && !(node.Dir == Direction.Down && node.Stretch >= maxStretch))
            {
                yield return node.Go(Direction.Down);
            }

            // Can move left.
            if (node.Col > 0 && node.Dir != Direction.Right && !(node.Dir == Direction.Left && node.Stretch >= maxStretch))
            {
                yield return node.Go(Direction.Left);
            }

            // Can move right.
            if (node.Col < Blocks[0].Length - 1 && node.Dir != Direction.Left && !(node.Dir == Direction.Right && node.Stretch >= maxStretch))
            {
                yield return node.Go(Direction.Right);
            }
        }

        // That's Dijkstra's algorithm.
        public long OptimalPath(bool part2 = false)
        {
            var queue = new PriorityQueue<Node, long>();
            var visited = new HashSet<Node>();
            var distances = new Dictionary<Node, long>();

            // Each node has four "coordinates".
            for (int line = 0; line < Blocks.Length; line++)
            {
                for (int col = 0; col < Blocks[0].Length; col++)
                {
                    foreach (Direction dir in Enum.GetValues(typeof(Direction)))
                    {
                        for (int stretch = 1; stretch <= (part2 ? 10 : 3); stretch++)
                        {
                            var node = new Node { Line = line, Col = col, Dir = dir, Stretch = stretch };
                            queue.Enqueue(node, long.MaxValue);
                            distances[node] = long.MaxValue;
                        }
                    }
                }
            }
            
            // First start: the block below (0, 0).
            var start = new Node { Line = 1, Col = 0, Dir = Direction.Down, Stretch = 1 };
            queue.Enqueue(start, Blocks[1][0]);
            distances[new Node { Line = 1, Col = 0, Dir = Direction.Down, Stretch = 1 }] = Blocks[1][0];

            // Second start: the block to the right of (0, 0).
            start = new Node { Line = 0, Col = 1, Dir = Direction.Right, Stretch = 1 };
            queue.Enqueue(start, Blocks[0][1]);
            distances[start] = Blocks[0][1];

            // We assign a value just so it's defined and C# doesn't complain later down,
            // but this value will always be overwitten.
            var current = new Node { Line = 0, Col = 0, Dir = Direction.Up, Stretch = 0 };
    
            while (queue.Count > 0)
            {
                // Uncomment to see the progress.
                // if (visited.Count % 100 == 0) Console.WriteLine(visited.Count);

                current = queue.Dequeue();

                // If we reached the end, stop right now.
                if (current.Line == Blocks.Length - 1 && current.Col == Blocks[0].Length - 1)
                {
                    if (!part2)
                    {
                        break;
                    }
                    else if (current.Stretch >= 4)
                    {
                        // In part 2, we can only stop after a stretch of at least 4.
                        break;
                    }
                }

                // It's a node we already visited
                // because we updated its distance to be lower.
                // (so we now encounter the one
                // with the larger distance before the update.)
                //
                // This is because we can't update
                // the priority with `PriorityQueue`,
                // so we have to leave the node with the old priority
                // in the queue.
                if (visited.Contains(current))
                {
                    continue;
                }

                visited.Add(current);

                foreach (var neighbour in AvailableNodes(current, part2))
                {
                    if (visited.Contains(neighbour))
                    {
                        continue;
                    }

                    var newDistance = distances[current] + Blocks[neighbour.Line][neighbour.Col];
                    if (newDistance < distances[neighbour])
                    {
                        // We enqueue the node with the updated priority.
                        // Since the priority will always decrease,
                        // it's OK if we leave the old one in the queue,
                        // we'll always encounter the new updated one before.
                        queue.Enqueue(neighbour, newDistance);
                        distances[neighbour] = newDistance;
                    }
                }
            }

            return distances[current];
        }
    }

    public enum Direction
    {
        Up,
        Right,
        Down,
        Left,
    }

    readonly record struct Node
    {
        public int Line { get; init; }
        public int Col { get; init; }
        public Direction Dir { get; init; }
        public int Stretch { get; init; }

        public Node Go(Direction heading)
        {
            var (line, col) = heading switch
            {
                Direction.Up => (Line - 1, Col),
                Direction.Down => (Line + 1, Col),
                Direction.Left => (Line, Col - 1),
                Direction.Right => (Line, Col + 1),
                _ => throw new UnreachableException()
            };
            var stretch = heading == Dir ? Stretch + 1 : 1;
            return new() { Line = line, Col = col, Dir = heading, Stretch = stretch };
        }
    }

    public (object, object) Solve(string input)
    {
        var map = new Map(input);
        return (Answer1(map), Answer2(map));
    }

    static long Answer1(Map map)
    {
        return map.OptimalPath();
    }

    static long Answer2(Map map)
    {
        return map.OptimalPath(part2: true);
    }
}
