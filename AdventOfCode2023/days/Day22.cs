using System.Diagnostics;

namespace Natrium729.AdventOfCode2023;

public class Day22 : IDay
{
    record struct Block
    {
        public int X { get; init; }
        public int Y { get; init; }
        public int Z { get; init; }
    }

    enum Axis
    {
        X,
        Y,
        Z,
    }

    class Brick
    {
        // `Lower end contains the block that has a smaller X/Y/Z
        // (The puzzle guarantees only one coordinate varies.)
        public Block LowerEnd { get; set; }
        public Block UpperEnd { get; set; }

        public HashSet<Brick> Supports { get; } = new();
        public HashSet<Brick> IsSupportedBy { get; } = new();

        // The axis to which the brick is parallel.
        public Axis Orientation
        {
            get
            {
                if (LowerEnd.X == UpperEnd.X && LowerEnd.Y == UpperEnd.Y && LowerEnd.Z == UpperEnd.Z)
                {
                    // We check Z first so that single-block bricks are considered vertical.
                    // Viewed from the top,
                    // they are indistinguishable from vertical bricks,
                    // and it will matter.
                    return Axis.Z;
                }

                if (LowerEnd.X != UpperEnd.X)
                {
                    return Axis.X;
                }

                if (LowerEnd.Y != UpperEnd.Y)
                {
                    return Axis.Y;
                }

                if (LowerEnd.Z != UpperEnd.Z)
                {
                    return Axis.Z;
                }

                throw new Exception(":'(");
            }
        }

        public Brick(string line)
        {
            var blocks = line.Split('~');

            var coords1 = Utils.SplitAtCommas(blocks[0]).Select(int.Parse).ToArray();
            var block1 = new Block { X = coords1[0], Y = coords1[1], Z = coords1[2] };

            var coords2 = Utils.SplitAtCommas(blocks[1]).Select(int.Parse).ToArray();
            var block2 = new Block { X = coords2[0], Y = coords2[1], Z = coords2[2] };

            if (block1.Z < block2.Z || block1.X < block2.X || block1.Y < block2.Y)
            {
                LowerEnd = block1;
                UpperEnd = block2;
            }
            else
            {
                LowerEnd = block2;
                UpperEnd = block1;
            }
        }

        Brick(Block lowerEnd, Block upperEnd)
        {
            LowerEnd = lowerEnd;
            UpperEnd = upperEnd;
        }

        public Brick Clone()
        {
            var brick = new Brick(LowerEnd, UpperEnd);
            // We clear the hash sets.
            // We'll recompute them later.
            brick.Supports.Clear();
            brick.IsSupportedBy.Clear();
            return brick;
        }

        public void DropTo(int z)
        {
            var dz = LowerEnd.Z - z;
            LowerEnd = LowerEnd with { Z = LowerEnd.Z - dz };
            UpperEnd = UpperEnd with { Z = UpperEnd.Z - dz };
        }

        // Determines if this bricks intersects with another as seen from the top,
        // irregardless of their Z coordinates.
        public bool IntersectsXY(Brick other)
        {
            // I first tried to do it mathematically,
            // but there was a mistake somewhere.
            // Since the number are small, we'll just bruteforce it.

            var xMin1 = Math.Min(LowerEnd.X, UpperEnd.X);
            var xMax1 = Math.Max(LowerEnd.X, UpperEnd.X);
            var yMin1 = Math.Min(LowerEnd.Y, UpperEnd.Y);
            var yMax1 = Math.Max(LowerEnd.Y, UpperEnd.Y);

            var xMin2 = Math.Min(other.LowerEnd.X, other.UpperEnd.X);
            var xMax2 = Math.Max(other.LowerEnd.X, other.UpperEnd.X);
            var yMin2 = Math.Min(other.LowerEnd.Y, other.UpperEnd.Y);
            var yMax2 = Math.Max(other.LowerEnd.Y, other.UpperEnd.Y);

            for (int x1 = xMin1; x1 <= xMax1; x1++)
            {
                for (int y1 = yMin1; y1 <= yMax1; y1++)
                {
                    for (int x2 = xMin2; x2 <= xMax2; x2++)
                    {
                        for (int y2 = yMin2; y2 <= yMax2; y2++)
                        {
                            if (x1 == x2 && y1 == y2)
                            {
                                return true;
                            }
                        }
                    }
                }
            }

            return false;
        }
    }

    // Returns the number of bricks that were dropped.
    static long DropBricks(Brick[] bricks)
    {
        var n = 0;

        var xMin = bricks.Select(brick => Math.Min(brick.LowerEnd.X, brick.UpperEnd.X)).Min();
        var xMax = bricks.Select(brick => Math.Max(brick.LowerEnd.X, brick.UpperEnd.X)).Max();
        var yMin = bricks.Select(brick => Math.Min(brick.LowerEnd.Y, brick.UpperEnd.Y)).Min();
        var yMax = bricks.Select(brick => Math.Max(brick.LowerEnd.Y, brick.UpperEnd.Y)).Max();

        var heightMap = new int[xMax - xMin + 1][];
        for (int x = 0; x < heightMap.Length; x++)
        {
            heightMap[x] = Enumerable.Repeat(0, yMax - yMin + 1).ToArray();
        }

        foreach (var brick in bricks)
        {
            var initialZ = brick.LowerEnd.Z;
            var max = 0;

            switch (brick.Orientation)
            {
                case Axis.Z:
                    brick.DropTo(heightMap[brick.LowerEnd.X][brick.LowerEnd.Y] + 1);
                    heightMap[brick.LowerEnd.X][brick.LowerEnd.Y] = brick.UpperEnd.Z;
                    break;

                case Axis.X:
                    max = 0;
                    for (int x = brick.LowerEnd.X; x <= brick.UpperEnd.X; x++)
                    {
                        max = Math.Max(max, heightMap[x][brick.LowerEnd.Y]);
                    }
                    brick.DropTo(max + 1);
                    for (int x = brick.LowerEnd.X; x <= brick.UpperEnd.X; x++)
                    {
                        heightMap[x][brick.LowerEnd.Y] = max + 1;
                    }
                    break;

                case Axis.Y:
                    max = 0;
                    for (int y = brick.LowerEnd.Y; y <= brick.UpperEnd.Y; y++)
                    {
                        max = Math.Max(max, heightMap[brick.LowerEnd.X][y]);
                    }
                    brick.DropTo(max + 1);
                    for (int y = brick.LowerEnd.Y; y <= brick.UpperEnd.Y; y++)
                    {
                        heightMap[brick.LowerEnd.X][y] = max + 1;
                    }
                    break;

                default:
                    throw new UnreachableException();
            }

            if (initialZ != brick.LowerEnd.Z)
            {
                n++;
            }
        }

        foreach (var brick in bricks)
        {
            foreach (var other in bricks)
            {
                if (brick.UpperEnd.Z + 1 == other.LowerEnd.Z && brick.IntersectsXY(other))
                {
                    brick.Supports.Add(other);
                    other.IsSupportedBy.Add(brick);
                }
            }
        }

        return n;
    }

    static Brick[] ParseInput(string input)
    {
        // We order them by altitude.
        // It will be easier to drop them.
        var bricks = (
            from line in Utils.SplitLines(input)
            let brick = new Brick(line)
            orderby brick.LowerEnd.Z
            select brick
        ).ToArray();

        // Both parts starts with dropping the bricks,
        // so we can do it right after parsing.
        DropBricks(bricks);

        return bricks;
    }

    public (object, object) Solve(string input)
    {
        var bricks = ParseInput(input);
        return (Answer1(bricks), Answer2(bricks));
    }

    static long Answer1(Brick[] bricks)
    {
        var cannotBeDisintegrated = new HashSet<Brick>();

        foreach (var brick in bricks)
        {
            if (brick.IsSupportedBy.Count == 1)
            {
                cannotBeDisintegrated.Add(brick.IsSupportedBy.First());
            }
        }

        return bricks.Length - cannotBeDisintegrated.Count;
    }

    static long Answer2(Brick[] bricks)
    {
        // Bruteforcing is a bit long, (as in, take quite a few seconds)
        // but it's tolerable.
        // Although it's still a bit sad
        // that I don't care enough to find something faster. :)
        long sum = 0;
        foreach (var brick in bricks)
        {
            var clonedWithoutCurrent = (
                from old in bricks
                where old != brick
                select old.Clone()
            ).ToArray();

            sum += DropBricks(clonedWithoutCurrent);
        }
        return sum;
    }
}
