namespace Natrium729.AdventOfCode2023;

public class Day4 : IDay
{
    class Card
    {
        public int Id { get; }

        public HashSet<int> WinningNumbers { get; }

        public HashSet<int> MyNumbers { get; }

        public int Matches { get; }

        public Card(string input)
        {
            string[] delimiters = { " ", ":" };
            var words = input.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);

            Id = int.Parse(words[1]);

            WinningNumbers = new HashSet<int>();
            MyNumbers = new HashSet<int>();

            var current = WinningNumbers;
            foreach (var word in words.Skip(2))
            {
                if (word == "|")
                {
                    current = MyNumbers;
                    continue;
                }

                current.Add(int.Parse(word));
            }

            Matches = WinningNumbers.Intersect(MyNumbers).Count();
        }
    }

    public (object, object) Solve(string input)
    {
        var cards = (
            from line in input.Split("\n", StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries)
            select new Card(line)
        ).ToArray();
        return (Answer1(cards), Answer2(cards));
    }


    static int Answer1(Card[] cards)
    {
        return (
            from card in cards
                // If the number of matches is 0 and thus the exponent, -1
                // the cast truncates the result of the `Pow` to 0,
                // so everything's good, I guess.
            select (int)Math.Pow(2, card.Matches - 1)
        ).Sum();
    }

    static int Answer2(Card[] cards)
    {
        var counts = Enumerable.Repeat(1, cards.Length).ToArray();
        foreach (var card in cards)
        {
            for (int i = card.Id; i < Math.Min(card.Id + card.Matches, cards.Length); i++)
            {
                counts[i] += counts[card.Id - 1];
            }
        }
        return counts.Sum();
    }
}
