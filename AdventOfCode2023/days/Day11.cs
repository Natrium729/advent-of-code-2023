namespace Natrium729.AdventOfCode2023;

public class Day11 : IDay
{
    class Image
    {
        public string[] Cells { get; }
        public List<(int, int)> Galaxies { get; } = new();
        public HashSet<int> EmptyLines { get; } = new();
        public HashSet<int> EmptyColumns { get; } = new();

        public Image(string input)
        {
            Cells = Utils.SplitLines(input);
            for (int y = 0; y < Cells.Length; y++)
            {
                var line = Cells[y];
                var lineIsEmpty = true;
                for (int x = 0; x < line.Length; x++)
                {
                    if (line[x] == '#')
                    {
                        lineIsEmpty = false;
                        Galaxies.Add((x, y));
                    }
                }

                if (lineIsEmpty)
                {
                    EmptyLines.Add(y);
                }
            }

            for (int x = 0; x < Cells[0].Length; x++)
            {
                var colIsEmpty = true;
                for (int y = 0; y < Cells.Length; y++)
                {
                    if (Cells[y][x] == '#')
                    {
                        colIsEmpty = false;
                    }
                }

                if (colIsEmpty)
                {
                    EmptyColumns.Add(x);
                }
            }
        }
    }

    public (object, object) Solve(string input)
    {
        var image = new Image(input);

        return (Answer1(image), Answer2(image));
    }

    static long Answer1(Image image)
    {
        var sum = 0L;

        for (int i = 0; i < image.Galaxies.Count; i++)
        {
            for (int j = i + 1; j < image.Galaxies.Count; j++)
            {
                var (x1, y1) = image.Galaxies[i];
                var (x2, y2) = image.Galaxies[j];

                sum += Math.Abs(x2 - x1) + Math.Abs(y2 - y1);

                sum += image.EmptyLines
                    .Where(line => Math.Min(y1, y2) < line && line < Math.Max(y1, y2))
                    .Count();

                sum += image.EmptyColumns
                    .Where(col => Math.Min(x1, x2) < col && col < Math.Max(x1, x2))
                    .Count();
            }
        }

        return sum;
    }

    static long Answer2(Image image)
    {
        // That's the same thing as part 1,
        // except we multiply the counts of empty lines/columns by 999 999.
        // (Not 1 000 000 because we already counted them once when computing the distance.)
        var sum = 0L;

        for (int i = 0; i < image.Galaxies.Count; i++)
        {
            for (int j = i + 1; j < image.Galaxies.Count; j++)
            {
                var (x1, y1) = image.Galaxies[i];
                var (x2, y2) = image.Galaxies[j];

                sum += Math.Abs(x2 - x1) + Math.Abs(y2 - y1);

                sum += 999_999 * image.EmptyLines
                    .Where(line => Math.Min(y1, y2) < line && line < Math.Max(y1, y2))
                    .Count();

                sum += 999_999 * image.EmptyColumns
                    .Where(col => Math.Min(x1, x2) < col && col < Math.Max(x1, x2))
                    .Count();
            }
        }

        return sum;
    }
}
