using System.Diagnostics;

namespace Natrium729.AdventOfCode2023;

public class Day21 : IDay
{
    int Steps { get; }

    public Day21(int steps = 64)
    {
        // Because the tests have less steps.
        Steps = steps;
    }

    class Map
    {
        char[][] Grid { get; }
        public int StartLine { get; }
        public int StartCol { get; }

        public Map(string input)
        {
            Grid = (
                from string line in Utils.SplitLines(input)
                select line.ToArray()
            ).ToArray();

            for (int line = 0; line < Grid.Length; line++)
            {
                for (int col = 0; col < Grid[line].Length; col++)
                {
                    if (Grid[line][col] == 'S')
                    {
                        StartLine = line;
                        StartCol = col;
                        Grid[line][col] = '.';
                        break;
                    }
                }
            }
        }

        public IEnumerable<(int, int)> Neighbours(int line, int col)
        {
            var d = new (int, int)[] { (-1, 0), (1, 0), (0, -1), (0, 1) };
            foreach (var (dLine, dCol) in d)
            {
                var otherLine = line + dLine;
                var otherCol = col + dCol;

                if (
                    otherLine < 0 || otherLine >= Grid.Length
                    || otherCol < 0 || otherCol >= Grid[otherLine].Length
                )
                {
                    continue;
                }

                var cell = Grid[otherLine][otherCol];

                // Can't go on rocks.
                if (cell == '#')
                {
                    continue;
                }

                yield return (otherLine, otherCol);
            }
        }
    }


        public (object, object) Solve(string input)
        {
            var map = new Map(input);

            return (Answer1(map), Answer2());
        }

        long Answer1(Map map)
        {
            var available = new HashSet<(int, int)>() { (map.StartLine, map.StartCol) };

            for (int i = 0; i < Steps; i++)
            {
                var nextAvailable = new HashSet<(int, int)>();
                foreach (var (line, col) in available)
                {
                    nextAvailable.UnionWith(map.Neighbours(line, col));
                }
                available = nextAvailable;
            }

            return available.Count;
        }

        static long Answer2()
        {
            return 0;
        }
    }
