using System.Diagnostics;

namespace Natrium729.AdventOfCode2023;

public class Day8 : IDay
{
    /// The test for the part 2 makes the answer 1 throw an exception,
    // so we add a way to skip it.
    public bool SkipPart1 = false;

    public (object, object) Solve(string input)
    {
        var pars = Utils.SplitPars(input);
        var dirs = pars[0];
        var nodes = (
            from line in Utils.SplitLines(pars[1])
            let parts = line.Split(" = ")
            let node = parts[0]
            let destinations = parts[1].Split(", ")
            let left = destinations[0][1..]
            let right = destinations[1][..^1]
            select (node, (left, right))
        ).ToDictionary(x => x.Item1, x => x.Item2);

        return (SkipPart1 ? 0 : Answer1(dirs, nodes), Answer2(dirs, nodes));
    }


    static int Answer1(string dirs, Dictionary<string, (string, string)> nodes)
    {
        var current = "AAA";
        var steps = 0;

        while (current != "ZZZ")
        {
            current = dirs[steps % dirs.Length] switch
            {
                'L' => nodes[current].Item1,
                'R' => nodes[current].Item2,
                _ => throw new UnreachableException()
            };
            steps++;
        }
        return steps;
    }

    static long Answer2(string dirs, Dictionary<string, (string, string)> nodes)
    {
        // This is exactly the kind of problem I hate.
        //
        // First, I write a solution that doesn't work in the general case
        // but does for the carefully crafted input from Advent of Code.
        // I still get the wrong answer because the int overflows.
        //
        // Because C# lets the int overflow without an exception,
        // I think that my code is wrong
        // and realise it doesn't work for the general case.
        // So I try to find a clever mathematical way
        // that works all the time but I fail.
        //
        // Then I inspect a bit the paths we get
        // and notice that each path only visits a single goal,
        // and visits it on a fixed interval.
        // So why my first solution,
        // which should succeed on this kind input, failed?
        // I search on the web a little and see that people used a long.
        // It dawns on me that the int overflowed.
        //
        // Basically, I hate the problems which you can solve
        // only because the input has some specific pattern,
        // Especially when you are so into thinking about the math behind it
        // that you overlook the overflow issue.

        var starts = nodes.Keys.Where(k => k.EndsWith('A')).ToArray();

        var zeds = new List<long>();
        foreach (var start in starts)
        {
            var current = start;
            var steps = 0;
            while (!current.EndsWith('Z'))
            {
                current = dirs[steps % dirs.Length] switch
                {
                    'L' => nodes[current].Item1,
                    'R' => nodes[current].Item2,
                    _ => throw new UnreachableException()
                };
                steps++;
            };

            zeds.Add(steps);
        }

        return zeds.Aggregate((a, b) => Utils.Lcm(a, b));
    }
}
