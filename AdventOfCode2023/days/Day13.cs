namespace Natrium729.AdventOfCode2023;

public class Day13 : IDay
{
    class Pattern
    {
        public char[][] Content { get; }

        public Pattern(string input)
        {
            Content = (
                from line in Utils.SplitLines(input)
                select line.ToArray()
            ).ToArray();
        }

        public (char[][], char[][]) HorizontalAxisSplit(int y)
        {
            var upper = Content.Take(y).ToArray();
            var lower = Content.Skip(y).ToArray();
            return (upper, lower);
        }

        public (char[][], char[][]) VerticalAxisSplit(int x)
        {
            var left = (
                from line in Content
                select line.Take(x).ToArray()
            ).ToArray();
            var right = (
                from line in Content
                select line.Skip(x).ToArray()
            ).ToArray();
            return (left, right);
        }
    }

    public (object, object) Solve(string input)
    {
        var patterns = (
            from par in Utils.SplitPars(input)
            select new Pattern(par)
        ).ToArray();

        return (Answer1(patterns), Answer2(patterns));
    }

    static long Answer1(Pattern[] patterns)
    {
        long horizontalSum = 0;
        long verticalSum = 0;

        foreach (var pattern in patterns)
        {
            // Vertical axis splits.

            for (int i = 1; i < pattern.Content[0].Length; i++)
            {
                var good = true;
                var (left, right) = pattern.VerticalAxisSplit(i);
                for (int y = 0; y < left.Length; y++)
                {
                    if (left[y].Reverse().Zip(right[y]).Any(x => x.First != x.Second))
                    {
                        good = false;
                        break;
                    }
                }

                if (good)
                {
                    verticalSum += i;
                    break;
                }
            }

            // Horizontal axis split.

            for (int i = 1; i < pattern.Content.Length; i++)
            {
                var good = true;
                var (upper, lower) = pattern.HorizontalAxisSplit(i);
                var height = Math.Min(upper.Length, lower.Length);

                for (int x = 0; x < upper[0].Length; x++)
                {
                    for (int y = 0; y < height; y++)
                    {
                        if (upper[i - 1 - y][x] != lower[y][x])
                        {
                            good = false;
                            break;
                        }
                    }
                }

                if (good)
                {
                    horizontalSum += i;
                    break;
                }
            }
        }

        return verticalSum + 100 * horizontalSum;
    }

    // It's basically a copy-paste of part 1,
    // except we count the numbers of differences
    // instead of using a boolean.
    static long Answer2(Pattern[] patterns)
    {
        long horizontalSum = 0;
        long verticalSum = 0;

        foreach (var pattern in patterns)
        {
            // Vertical axis splits.

            for (int i = 1; i < pattern.Content[0].Length; i++)
            {
                var smudges = 0;
                var (left, right) = pattern.VerticalAxisSplit(i);
                for (int y = 0; y < left.Length; y++)
                {
                    // We could add an inner loop instead of counting all the line
                    // so that we could break earlier.
                    // But it's already enough fast without this optimisation.
                    smudges += left[y].Reverse().Zip(right[y]).Count(x => x.First != x.Second);
                    if (smudges > 1)
                    {
                        break;
                    }
                }

                if (smudges == 1)
                {
                    verticalSum += i;
                    break;
                }
            }

            // Horizontal axis split.

            for (int i = 1; i < pattern.Content.Length; i++)
            {
                var smudges = 0;
                var (upper, lower) = pattern.HorizontalAxisSplit(i);
                var height = Math.Min(upper.Length, lower.Length);

                for (int x = 0; x < upper[0].Length; x++)
                {
                    for (int y = 0; y < height; y++)
                    {
                        if (upper[i - 1 - y][x] != lower[y][x])
                        {
                            smudges += 1;
                        }

                        if (smudges > 1)
                        {
                            break;
                        }
                    }
                }

                if (smudges == 1)
                {
                    horizontalSum += i;
                    break;
                }
            }
        }

        return verticalSum + 100 * horizontalSum;
    }
}
