namespace Natrium729.AdventOfCode2023;

public class Day14 : IDay
{
    class Platform
    {
        public char[][] Content { get; }

        public Platform(string input)
        {
            Content = (
                from line in Utils.SplitLines(input)
                select line.ToArray()
            ).ToArray();
        }

        public void Print()
        {
            foreach (var line in Content)
            {
                foreach (var ch in line)
                {
                    Console.Write(ch);
                }
                Console.WriteLine();
            }
            Console.WriteLine();
        }

        // This was the original `NorthLoad` method.
        // That's me being clever and computing the north load
        // if we were tilting north, without actually moving the rocks north.
        //
        // Works well for part 1, but if we use it for part 2,
        // then we compute the load
        // as if we were tilting one last time north per cycle.
        // Obviously, it gives the wrong answer.
        public long NorthLoadIfTiltedNorth()
        {
            var load = 0L;

            for (int x = 0; x < Content[0].Length; x++)
            {
                int? topMostFreeSpace = null;
                var roundRocks = 0;

                for (int y = 0; y < Content.Length; y++)
                {
                    var cell = Content[y][x];

                    if (topMostFreeSpace == null && cell == '.')
                    {
                        topMostFreeSpace = y;
                    }

                    if (cell == '#')
                    {
                        // We can't name the variable `top`
                        // because of the variable with the same name after the loops.
                        // But in my opnion, they are not in the same scope
                        // so it should be allowed.
                        if (topMostFreeSpace is int topp)
                        {
                            for (int i = 0; i < roundRocks; i++)
                            {
                                load += Content.Length - (topp + i);
                            }
                            topMostFreeSpace = null;
                            roundRocks = 0;
                        }
                    }

                    if (cell == 'O')
                    {
                        if (topMostFreeSpace == null)
                        {
                            load += Content.Length - y;
                        }
                        else
                        {
                            roundRocks++;
                        }
                    }
                }

                if (topMostFreeSpace is int top)
                {
                    for (int i = 0; i < roundRocks; i++)
                    {
                        load += Content.Length - (top + i);
                    }
                }
            }

            return load;
        }

        public long NorthLoad()
        {
            var load = 0L;

            for (int y = 0; y < Content.Length; y++)
            {
                for (int x = 0; x < Content[0].Length; x++)
                {
                    if (Content[y][x] == 'O')
                    {
                        load += Content.Length - y;
                    }
                }
            }

            return load;
        }

        // All 4 tilting method are just copy-pastes with minor edits.
        // I'm sure we could use a single function while rotating the platform.
        // Less efficient (I suppose), but less prone to error.

        public void TiltNorth()
        {
            for (int x = 0; x < Content[0].Length; x++)
            {
                int? topMostFreeSpace = null;
                var roundRocks = 0;

                for (int y = 0; y < Content.Length; y++)
                {
                    var cell = Content[y][x];

                    if (topMostFreeSpace == null && cell == '.')
                    {
                        topMostFreeSpace = y;
                    }

                    if (cell == '#')
                    {
                        // We can't name the variable `top`
                        // because of the variable with the same name after the loops.
                        // But in my opnion, they are not in the same scope
                        // so it should be allowed.
                        if (topMostFreeSpace is int topp)
                        {
                            for (int i = 0; i < roundRocks; i++)
                            {
                                Content[topp + i][x] = 'O';
                            }
                            topMostFreeSpace = null;
                            roundRocks = 0;
                        }
                    }

                    if (cell == 'O')
                    {
                        if (topMostFreeSpace != null)
                        {
                            roundRocks++;
                            Content[y][x] = '.';
                        }
                    }
                }

                if (topMostFreeSpace is int top)
                {
                    for (int i = 0; i < roundRocks; i++)
                    {
                        Content[top + i][x] = 'O';
                    }
                }
            }
        }

        void TiltSouth()
        {
            for (int x = 0; x < Content[0].Length; x++)
            {
                int? bottomMostFreeSpace = null;
                var roundRocks = 0;

                for (int y = Content.Length - 1; y >= 0; y--)
                {
                    var cell = Content[y][x];

                    if (bottomMostFreeSpace == null && cell == '.')
                    {
                        bottomMostFreeSpace = y;
                    }

                    if (cell == '#')
                    {
                        // We can't name the variable `botomm`
                        // because of the variable with the same name after the loops.
                        // But in my opnion, they are not in the same scope
                        // so it should be allowed.
                        if (bottomMostFreeSpace is int bottomm)
                        {
                            for (int i = 0; i < roundRocks; i++)
                            {
                                Content[bottomm - i][x] = 'O';
                            }
                            bottomMostFreeSpace = null;
                            roundRocks = 0;
                        }
                    }

                    if (cell == 'O')
                    {
                        if (bottomMostFreeSpace != null)
                        {
                            roundRocks++;
                            Content[y][x] = '.';
                        }
                    }
                }

                if (bottomMostFreeSpace is int bottom)
                {
                    for (int i = 0; i < roundRocks; i++)
                    {
                        Content[bottom - i][x] = 'O';
                    }
                }
            }
        }

        void TiltWest()
        {
            for (int y = 0; y < Content.Length; y++)
            {
                int? leftMostFreeSpace = null;
                var roundRocks = 0;

                for (int x = 0; x < Content[0].Length; x++)
                {
                    var cell = Content[y][x];

                    if (leftMostFreeSpace == null && cell == '.')
                    {
                        leftMostFreeSpace = x;
                    }

                    if (cell == '#')
                    {
                        // We can't name the variable `left`
                        // because of the variable with the same name after the loops.
                        // But in my opnion, they are not in the same scope
                        // so it should be allowed.
                        if (leftMostFreeSpace is int leftt)
                        {
                            for (int i = 0; i < roundRocks; i++)
                            {
                                Content[y][leftt + i] = 'O';
                            }
                            leftMostFreeSpace = null;
                            roundRocks = 0;
                        }
                    }

                    if (cell == 'O')
                    {
                        if (leftMostFreeSpace != null)
                        {
                            roundRocks++;
                            Content[y][x] = '.';
                        }
                    }
                }

                if (leftMostFreeSpace is int left)
                {
                    for (int i = 0; i < roundRocks; i++)
                    {
                        Content[y][left + i] = 'O';
                    }
                }
            }
        }

        void TiltEast()
        {
            for (int y = 0; y < Content.Length; y++)
            {
                int? rightMostFreeSpace = null;
                var roundRocks = 0;

                for (int x = Content[0].Length - 1; x >= 0; x--)
                {
                    var cell = Content[y][x];

                    if (rightMostFreeSpace == null && cell == '.')
                    {
                        rightMostFreeSpace = x;
                    }

                    if (cell == '#')
                    {
                        // We can't name the variable `rightt`
                        // because of the variable with the same name after the loops.
                        // But in my opnion, they are not in the same scope
                        // so it should be allowed.
                        if (rightMostFreeSpace is int rightt)
                        {
                            for (int i = 0; i < roundRocks; i++)
                            {
                                Content[y][rightt - i] = 'O';
                            }
                            rightMostFreeSpace = null;
                            roundRocks = 0;
                        }
                    }

                    if (cell == 'O')
                    {
                        if (rightMostFreeSpace != null)
                        {
                            roundRocks++;
                            Content[y][x] = '.';
                        }
                    }
                }

                if (rightMostFreeSpace is int right)
                {
                    for (int i = 0; i < roundRocks; i++)
                    {
                        Content[y][right - i] = 'O';
                    }
                }
            }
        }

        public long Spin(int n)
        {
            // Map from state to first iteration we saw it.
            var states = new Dictionary<string, int>();
            // Map from iteration to load.
            var loads = new Dictionary<int, long>();

            for (int i = 0; i < n; i++)
            {
                TiltNorth();
                TiltWest();
                TiltSouth();
                TiltEast();

                var serialised = string.Join("", Content.Select(line => string.Concat(line)));
                if (states.TryGetValue(serialised, out var lastI))
                {
                    // We detected a cycle.
                    // No need to spin further.
                    var period = i - lastI;
                    var pos = lastI + ((n - lastI) % period) - 1;
                    var result = loads[pos];
                    return result;
                }
                else
                {
                    states.Add(serialised, i);
                    loads.Add(i, NorthLoad());
                }
            }

            // Not relevant with our problem,
            // But in case we spin for less than a cycle.
            return NorthLoad();
        }
    }

    public (object, object) Solve(string input)
    {
        var platform = new Platform(input);
        return (Answer1(platform), Answer2(platform));
    }

    static long Answer1(Platform platform)
    {
        return platform.NorthLoadIfTiltedNorth();
    }

    static long Answer2(Platform platform)
    {
        return platform.Spin(1_000_000_000);
    }
}
