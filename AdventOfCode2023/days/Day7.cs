using System.Diagnostics;

namespace Natrium729.AdventOfCode2023;

public class Day7 : IDay
{
    enum Card
    {
        Two,
        Three,
        Four,
        Five,
        Six,
        Seven,
        Eight,
        Nine,
        Ten,
        J,
        Q,
        K,
        A,
    }

    enum Type
    {
        HighCard,
        OnePair,
        TwoPair,
        ThreeOfAKind,
        FullHouse,
        FourOfAKind,
        FiveOfAKind,
    }

    static Card CharToCard(char ch)
    {
        return ch switch
        {
            '2' => Card.Two,
            '3' => Card.Three,
            '4' => Card.Four,
            '5' => Card.Five,
            '6' => Card.Six,
            '7' => Card.Seven,
            '8' => Card.Eight,
            '9' => Card.Nine,
            'T' => Card.Ten,
            'J' => Card.J,
            'Q' => Card.Q,
            'K' => Card.K,
            'A' => Card.A,
            _ => throw new ArgumentOutOfRangeException(nameof(ch), "Invalid char in hand.")
        };
    }

    class Hand
    {
        Card[] Cards { get; }

        public Hand(string cards)
        {
            if (cards.Length != 5)
            {
                throw new ArgumentException("A hand should have 5 cards.");
            }
            Cards = (
                from ch in cards
                select CharToCard(ch)
            ).ToArray();
        }

        // Can't name the method `Type` because it then shadows the enum,
        // nor `GetType` because `object` already has a method with this name.
        public Type Strength()
        {

            // We only need a length of 13,
            // But we'll waste one item so that the indices match the cards.
            var counts = new int[14];

            foreach (var card in Cards)
            {
                var i = card switch
                {
                    Card.A => 1,
                    Card.Two => 2,
                    Card.Three => 3,
                    Card.Four => 4,
                    Card.Five => 5,
                    Card.Six => 6,
                    Card.Seven => 7,
                    Card.Eight => 8,
                    Card.Nine => 9,
                    Card.Ten => 10,
                    Card.J => 11,
                    Card.Q => 12,
                    Card.K => 13,
                    _ => throw new UnreachableException()
                };
                counts[i]++;
            }

            // By using using `Any`/`Count` multiple times,
            // we run through the array multiple times, but that's not too bad.

            if (counts.Any(n => n == 5))
            {
                return Type.FiveOfAKind;
            }

            if (counts.Any(n => n == 4))
            {
                return Type.FourOfAKind;
            }

            var pairs = counts.Count(n => n == 2);

            if (counts.Any(n => n == 3))
            {
                if (pairs == 1)
                {
                    return Type.FullHouse;
                }

                return Type.ThreeOfAKind;
            }

            return pairs switch
            {
                2 => Type.TwoPair,
                1 => Type.OnePair,
                _ => Type.HighCard,
            };
        }

        Type StrengthJoker()
        {
            // We only need a length of 13,
            // But we'll waste one item so that the indices match the cards.
            var counts = new int[14];

            foreach (var card in Cards)
            {
                var i = card switch
                {
                    Card.A => 1,
                    Card.Two => 2,
                    Card.Three => 3,
                    Card.Four => 4,
                    Card.Five => 5,
                    Card.Six => 6,
                    Card.Seven => 7,
                    Card.Eight => 8,
                    Card.Nine => 9,
                    Card.Ten => 10,
                    Card.J => 11,
                    Card.Q => 12,
                    Card.K => 13,
                    _ => throw new UnreachableException()
                };
                counts[i]++;
            }

            // By using using `Any`/`Count` multiple times,
            // we run through the array multiple times, but that's not too bad.

            switch (counts[11])
            {
                case 5: return Type.FiveOfAKind;

                case 4: return Type.FiveOfAKind;

                case 3:
                    {
                        if (counts.Any(n => n == 2))
                        {
                            return Type.FiveOfAKind;
                        }
                        return Type.FourOfAKind;
                    }

                case 2:
                    {
                        if (counts.Any(n => n == 3))
                        {
                            return Type.FiveOfAKind;
                        }
                        if (counts.Count(n => n == 2) == 2)
                        {
                            // One of the pair is a pair of jokers.
                            return Type.FourOfAKind;
                        }
                        return Type.ThreeOfAKind;
                    }

                case 1:
                    {
                        if (counts.Any(n => n == 4))
                        {
                            return Type.FiveOfAKind;
                        }

                        if (counts.Any(n => n == 3))
                        {
                            return Type.FourOfAKind;
                        }

                        var pairs = counts.Count(n => n == 2);
                        if (pairs == 2)
                        {
                            return Type.FullHouse;
                        }
                        else if (pairs == 1)
                        {
                            return Type.ThreeOfAKind;
                        }
                        return Type.OnePair;
                    }

                // No jokers: use the normal strength.
                // It will recount the cards uselessly,
                // but we can live with that.
                case 0: return Strength();

                default: throw new UnreachableException();
            }
        }

        public int Compare(Hand other, bool joker = false)
        {
            Type thisType;
            Type otherType;
            if (joker)
            {
                thisType = StrengthJoker();
                otherType = other.StrengthJoker();
            }
            else
            {
                thisType = Strength();
                otherType = other.Strength();
            }

            if (thisType > otherType)
            {
                return 1;
            }
            else if (thisType < otherType)
            {
                return -1;
            }
            else
            {
                foreach (var (card, otherCard) in Cards.Zip(other.Cards))
                {
                    if (joker)
                    {
                        if (card == Card.J && otherCard != Card.J)
                        {
                            return -1;
                        }
                        else if (card != Card.J && otherCard == Card.J)
                        {
                            return 1;
                        }
                    }

                    if (card > otherCard)
                    {
                        return 1;
                    }
                    else if (card < otherCard)
                    {
                        return -1;
                    }
                }

                return 0;
            }
        }
    }

    public (object, object) Solve(string input)
    {
        var hands = (
            from line in Utils.SplitLines(input)
            let words = Utils.SplitWords(line)
            select (new Hand(words[0]), int.Parse(words[1]))
        ).ToArray();
        return (Answer1(hands), Answer2(hands));
    }


    static int Answer1((Hand, int)[] hands)
    {
        Array.Sort(hands, (a, b) => a.Item1.Compare(b.Item1));
        return (
            from x in Enumerable.Range(1, hands.Length).Zip(hands)
            let i = x.First
            let hand = x.Second.Item1
            let bid = x.Second.Item2
            select i * bid
        ).Sum();
    }

    static int Answer2((Hand, int)[] hands)
    {
        Array.Sort(hands, (a, b) => a.Item1.Compare(b.Item1, joker: true));
        return (
            from x in Enumerable.Range(1, hands.Length).Zip(hands)
            let i = x.First
            let hand = x.Second.Item1
            let bid = x.Second.Item2
            select i * bid
        ).Sum();
    }
}
