using System.Diagnostics;

namespace Natrium729.AdventOfCode2023;

public class Day18 : IDay
{
    public enum Direction
    {
        Up,
        Right,
        Down,
        Left,
    }

    record struct Instruction
    {
        public Direction Dir;
        public long Distance;

        public Instruction(string line, bool part2 = false)
        {
            var words = Utils.SplitWords(line);

            if (!part2)
            {

                Dir = words[0] switch
                {
                    "U" => Direction.Up,
                    "D" => Direction.Down,
                    "L" => Direction.Left,
                    "R" => Direction.Right,
                    _ => throw new UnreachableException()
                };

                Distance = long.Parse(words[1]);
            }
            else
            {
                Distance = Convert.ToInt64(words[2][2..^2], 16);
                Dir = words[2][^2..^1] switch
                {
                    "0" => Direction.Right,
                    "1" => Direction.Down,
                    "2" => Direction.Left,
                    "3" => Direction.Up,
                    _ => throw new UnreachableException()
                };
            }
        }
    }

    class Plan
    {
        public long EdgeLength { get; }
        long Area { get; }

        public Plan(string input, bool part2 = false)
        {
            var instructions = (
                from line in Utils.SplitLines(input)
                select new Instruction(line, part2)
            ).ToArray();

            var current = (0L, 0L);

            // The area is computed with the shoelace formula.

            foreach (var inst in instructions)
            {
                EdgeLength += inst.Distance;

                var next = inst.Dir switch
                {
                    Direction.Up => (current.Item1 - inst.Distance, current.Item2),
                    Direction.Down => (current.Item1 + inst.Distance, current.Item2),
                    Direction.Left => (current.Item1, current.Item2 - inst.Distance),
                    Direction.Right => (current.Item1, current.Item2 + inst.Distance),
                    _ => throw new UnreachableException()
                };

                Area += (current.Item1 + next.Item1) * (current.Item2 - next.Item2);
                current = next;
            }

            Area = Math.Abs(Area / 2);
        }

        public long InteriorCount()
        {
            // Picks's theorem.
            return Area - EdgeLength / 2 + 1;
        }
    }

    public (object, object) Solve(string input)
    {
        var plan = new Plan(input);
        var plan2 = new Plan(input, true);
        return (Answer1(plan), Answer2(plan2));
    }

    static long Answer1(Plan plan)
    {
        return plan.EdgeLength + plan.InteriorCount();
    }

    static long Answer2(Plan plan)
    {
        return plan.EdgeLength + plan.InteriorCount();
    }
}
