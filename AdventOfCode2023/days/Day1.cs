using System.Text.RegularExpressions;

namespace Natrium729.AdventOfCode2023;

public partial class Day1 : IDay
{
    public (object, object) Solve(string input)
    {
        return (Answer1(input), Answer2(input));
    }

    static int Answer1(string input)
    {
        var calibrationValues =
            from line in input.Split("\n")
            where line.Trim() != ""
            let digits = (
                from ch in line
                where '0' <= ch && ch <= '9'
                select ch
            ).ToArray()
            where digits.Length > 0
            select (digits[0] - '0') * 10 + (digits[^1] - '0');

        return calibrationValues.Sum();
    }

    // I used the quick fix from VS Code to generate this.
    // I have absolutely no idea how one could find it otherwise.
    [GeneratedRegex("[0-9]|zero|one|two|three|four|five|six|seven|eight|nine")]
    private static partial Regex DigitRegex();

    static int Answer2(string input)
    {
        // We could parse the input with a trie or something,
        // but we'll just use a regex.
        var calibrationValues =
            from line in input.Split("\n")
            where line.Trim() != ""
            let digits = (
                from m in DigitRegex().Matches(line)
                select AsDigit(m.Value)
            ).ToArray()
            select digits[0] * 10 + digits[^1];

        return calibrationValues.Sum();
    }

    static int AsDigit(string val)
    {
        if (val.Length == 1)
        {
            var ch = val[0];
            if ('0' <= ch && ch <= '9')
            {
                return val[0] - '0';
            }
            else
            {
                throw new ArgumentException("The given string is not a digit.");
            }
        }

        return val switch
        {
            "zero" => 0,
            "one" => 1,
            "two" => 2,
            "three" => 3,
            "four" => 4,
            "five" => 5,
            "six" => 6,
            "seven" => 7,
            "eight" => 8,
            "nine" => 9,
            _ => throw new ArgumentException("The given string is not a digit.")
        };
    }
}
