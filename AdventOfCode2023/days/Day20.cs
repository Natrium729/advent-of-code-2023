using System.Diagnostics;

namespace Natrium729.AdventOfCode2023;

public class Day20 : IDay
{
    abstract class Module
    {
        public string Name { get; }
        public bool? NextPulse { get; set; } = null;
        public List<Module> Destinations { get; } = new();

        // Will contain the modules that still have a pulse to send,
        // and the pulse they will send.
        // It's static so we won't be able to have multiple networks of modules
        // running at the same time, but we won't need it.
        public static Queue<(Module, bool)> Q { get; } = new();

        // Returns the number of times low and high pulses have been sent.
        public static (long, long) PushButton(Dictionary<string, Module> modules)
        {
            // For the initial pulse the button sends.
            long low = 1;
            long high = 0;

            // Start by sending a low pulse to the broadcaster.
            // The source doesn't matter for the broadcaster.
            var broadcaster = modules["broadcaster"];
            if (broadcaster.ReceivePulse(broadcaster, false) is bool p)
            {
                // Will always succeed, but let's do it properly anyway.
                Q.Enqueue((broadcaster, p));
            };

            while (Q.TryDequeue(out var next))
            {
                var nextModule = next.Item1;
                var nextPulse = next.Item2;
                // `SendPulse` will enqueue other values.
                var n = nextModule.SendPulse(nextPulse);
                if (nextPulse)
                {
                    high += n;
                }
                else
                {
                    low += n;
                }
            }

            return (low, high);

        }

        public Module(string name)
        {
            // Since the name will be the key in a dict of modules,
            // keeping it in the module is mostly useful for debugging,
            Name = name;
        }

        public void RegisterDestinationModule(Module dest)
        {
            Destinations.Add(dest);
        }

        // Returns the number of times the pulse was sent.
        public long SendPulse(bool pulse)
        {
            long n = 0;

            foreach (var dest in Destinations)
            {
                var nextPulse = dest.ReceivePulse(this, pulse);
                if (nextPulse is bool p)
                {
                    Q.Enqueue((dest, p));
                }
                n++;
            }

            return n;
        }

        public abstract void RegisterInputModule(Module input);

        // Returns the next pulse that this module will send
        // when receiving the given pulse from `input`.
        public abstract bool? ReceivePulse(Module input, bool pulse);

    }

    class Broadcaster : Module
    {
        public Broadcaster(string name) : base(name)
        {
        }

        public override void RegisterInputModule(Module input)
        {
            // Do nothing.
        }

        public override bool? ReceivePulse(Module input, bool pulse)
        {
            return pulse;
        }
    }

    class FlipFlop : Module
    {
        // Flip-flops are initially off.
        bool State { get; set; } = false;

        public FlipFlop(string name) : base(name)
        {
        }

        public override void RegisterInputModule(Module input)
        {
            // Do nothing.
        }

        public override bool? ReceivePulse(Module input, bool pulse)
        {
            if (pulse)
            {
                return null;
            }
            else
            {
                State = !State;
                return State;
            }
        }
    }

    class Conjunction : Module
    {
        public Dictionary<Module, bool> RememberedInputs { get; } = new();

        public bool EmittedLow { get; set; } = false;

        public Conjunction(string name) : base(name)
        {
        }

        public override void RegisterInputModule(Module input)
        {
            RememberedInputs.Add(input, false);
        }

        public override bool? ReceivePulse(Module input, bool pulse)
        {
            RememberedInputs[input] = pulse;
            var nextPulse = !RememberedInputs.Values.All(val => val);
            if (!nextPulse)
            {
                EmittedLow = true;
            }
            return nextPulse;
        }
    }

    // Modules that only appears in destinations.
    class UselessModule : Module
    {
        public UselessModule(string name) : base(name)
        {
        }

        public override void RegisterInputModule(Module input)
        {
            // Do nothing.
        }

        public override bool? ReceivePulse(Module input, bool pulse)
        {
            return null;
        }
    }

    static Dictionary<string, Module> ParseInput(string input)
    {
        var lines =
            from line in Utils.SplitLines(input)
            select line.Split(" -> ");

        var modules = new Dictionary<string, Module>();

        // Collect all the modules.
        foreach (var line in lines)
        {
            var source = line[0];
            if (source == "broadcaster")
            {
                modules.Add(source, new Broadcaster(source));
            }
            else
            {
                var name = source[1..];
                Module module = source[0] switch
                {
                    '%' => new FlipFlop(name),
                    '&' => new Conjunction(name),
                    _ => throw new UnreachableException()
                };
                modules.Add(name, module);
            }
        }

        // Set the inputs and destinations of all the modules collected earlier.
        foreach (var line in lines)
        {
            var sourceName = line[0];
            if (sourceName != "broadcaster")
            {
                sourceName = sourceName[1..];
            }
            var source = modules[sourceName];
            var destinations = Utils.SplitAtCommas(line[1]);
            foreach (var destName in destinations)
            {
                // Add the destination as a useless module
                // if it has not been collected as a source.
                modules.TryAdd(destName, new UselessModule(destName));

                var dest = modules[destName];
                source.RegisterDestinationModule(dest);
                dest.RegisterInputModule(source);
            }
        }

        return modules;
    }

    public (object, object) Solve(string input)
    {
        // We parse the input twice
        // so that each part has a fresh set of modules with default state.
        return (Answer1(ParseInput(input)), Answer2(ParseInput(input)));
    }

    static long Answer1(Dictionary<string, Module> modules)
    {
        long lowSum = 0;
        long highSum = 0;

        for (int i = 0; i < 1000; i++)
        {
            var (low, high) = Module.PushButton(modules);
            lowSum += low;
            highSum += high;
        }

        return lowSum * highSum;
    }

    static long Answer2(Dictionary<string, Module> modules)
    {
        if (modules.TryGetValue("rx", out var rx))
        {
            // Will be overwritten below (we know it).
            // We also assume there'll be only one.
            Module parent = modules["rx"];
            foreach (var item in modules)
            {
                if (item.Value.Destinations.Contains(rx))
                {
                    parent = item.Value;
                    if (parent is not Conjunction)
                    {
                        throw new Exception("We assumed wrongly that the input of rx is a conjunction.");
                    }
                }
            }

            // We collect all the inputs of the inputs of the input of rx,
            // and assume they are all, in both generation, conjunctions modules
            var grandparents = new HashSet<Module>();
            var greatGrandparents = new Dictionary<Conjunction, long>();

            foreach (var item in modules)
            {
                if (item.Value.Destinations.Contains(parent))
                {
                    grandparents.Add(item.Value);
                    if (item.Value is not Conjunction)
                    {
                        throw new Exception("We assumed wrongly that an input of the input of rx is a conjunction.");
                    }

                    foreach (var item2 in modules)
                    {
                        if (item2.Value.Destinations.Contains(item.Value))
                        {
                            if (item2.Value is Conjunction conj)
                            {
                                greatGrandparents.Add(conj, 0);
                            }
                            else
                            {
                                throw new Exception("We assumed wrongly that an input of an input of the input of rx is a conjunction.");
                            }
                        }
                    }
                }
            }

            // We'll assume a lot of things.
            //
            // We look for when the great-grandparents emit a low pulse
            // (if they and the grandparents are conjunctions, as we assumed,
            // then rx will receive a low pulse
            // when all its great-grandparents emit a low pulse at the same time),
            // keep track of the iteration when it happens,
            // assume it's the end of a cycle, and stop when we found them all.
            //
            // Then we compute the LCM of all the cycle lengths.
            long i = 0;
            while (true)
            {
                i++;
                Module.PushButton(modules);

                foreach (var item in greatGrandparents)
                {
                    if (item.Value == 0 && item.Key.EmittedLow)
                    {
                        greatGrandparents[item.Key] = i;
                    }
                }

                if (greatGrandparents.Values.All(n => n > 0))
                {
                    break;
                }
            }

            return greatGrandparents.Values.Aggregate(Utils.Lcm);
        }
        else
        {
            // Skip part 2 for tests.
            return 0;
        }
    }
}
