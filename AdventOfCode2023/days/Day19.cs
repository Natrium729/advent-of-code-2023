using System.Diagnostics;

namespace Natrium729.AdventOfCode2023;

public class Day19 : IDay
{
    enum Category
    {
        X,
        M,
        A,
        S,
    }

    readonly record struct Part
    {
        public long X { get; init; }
        public long M { get; init; }
        public long A { get; init; }
        public long S { get; init; }

        public Part(string str)
        {
            var categories =
                from category in Utils.SplitAtCommas(str[1..^1]) // Remove braces.
                select category.Split('=', StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries);

            foreach (var category in categories)
            {
                switch (category[0])
                {
                    case "x": X = long.Parse(category[1]); break;
                    case "m": M = long.Parse(category[1]); break;
                    case "a": A = long.Parse(category[1]); break;
                    case "s": S = long.Parse(category[1]); break;
                    default: throw new UnreachableException();
                }
            }
        }

        public Part(long x, long m, long a, long s)
        {
            X = x;
            M = m;
            A = a;
            S = s;
        }
    }

    enum Operation
    {
        Less,
        Greater,
        True,
    }

    // I could have made a different class for each operation,
    // but it's more straightforward with a single class.
    class Rule
    {
        public Operation Op { get; }
        public long Value { get; }
        public Category Cat { get; }
        public string Dest { get; }

        public Rule(string str)
        {
            var split = str.Split(":");

            if (split.Length == 1)
            {
                Op = Operation.True;
                Value = -1; // Doesn't matter.
                Cat = Category.X; // Doesn't matter.
                Dest = split[0];
                return;
            }

            Dest = split[1];
            var condition = split[0];

            Cat = condition[0] switch
            {
                'x' => Category.X,
                'm' => Category.M,
                'a' => Category.A,
                's' => Category.S,
                _ => throw new UnreachableException()
            };

            Op = condition[1] switch
            {
                '<' => Operation.Less,
                '>' => Operation.Greater,
                // The `True` operation has been handled at the beginning.
                _ => throw new UnreachableException()
            };

            Value = long.Parse(condition[2..]);
        }

        public Rule(Operation op, long val, Category cat, string dest)
        {
            Op = op;
            Value = val;
            Cat = cat;
            Dest = dest;
        }

        // Returns the destination if the rule succeeds.
        // Returns `null` if the rule fails.
        // (I have the feeling I'm writing some Inform 7, ha ha!)
        public string? Follow(Part part)
        {
            var tested = Cat switch
            {
                Category.X => part.X,
                Category.M => part.M,
                Category.A => part.A,
                Category.S => part.S,
                _ => throw new UnreachableException()
            };

            var success = Op switch
            {
                Operation.Less => tested < Value,
                Operation.Greater => tested > Value,
                Operation.True => true,
                _ => throw new UnreachableException()
            };

            return success ? Dest : null;
        }

        // Given a range of values for X, M, A and S,
        // what subset of this range will make the rule succeed?
        // (The range is inclusive.)
        public (Part, Part) MakesSucceed(Part min, Part max)
        {
            var (valMin, valMax) = Cat switch
            {
                Category.X => (min.X, max.X),
                Category.M => (min.M, max.M),
                Category.A => (min.A, max.A),
                Category.S => (min.S, max.S),
                _ => throw new UnreachableException()
            };

            switch (Op)
            {
                case Operation.True:
                    // Anything makes the rule succeed.
                    return (min, max);
                case Operation.Less:
                    if (valMin >= Value)
                    {
                        valMin = 0;
                        valMax = 0;
                    }
                    else
                    {
                        valMax = Math.Min(valMax, Value - 1);
                    }
                    break;
                case Operation.Greater:
                    if (valMax <= Value)
                    {
                        valMin = 0;
                        valMax = 0;
                    }
                    else
                    {
                        valMin = Math.Max(valMin, Value + 1);
                    }
                    break;
                default: throw new UnreachableException();
            }

            return Cat switch
            {
                Category.X => (min with { X = valMin }, max with { X = valMax }),
                Category.M => (min with { M = valMin }, max with { M = valMax }),
                Category.A => (min with { A = valMin }, max with { A = valMax }),
                Category.S => (min with { S = valMin }, max with { S = valMax }),
                _ => throw new UnreachableException(),
            };
        }

        // Given a range of values for X, M, A and S,
        // what subset of this range will make the rule fail?
        // (The range is inclusive.)
        public (Part, Part) MakesFail(Part min, Part max)
        {
            var (valMin, valMax) = Cat switch
            {
                Category.X => (min.X, max.X),
                Category.M => (min.M, max.M),
                Category.A => (min.A, max.A),
                Category.S => (min.S, max.S),
                _ => throw new UnreachableException()
            };

            switch (Op)
            {
                case Operation.True:
                    // Nothing can make the rule fail.
                    valMin = 0;
                    valMax = 0;
                    break;
                case Operation.Less:
                    if (valMax < Value)
                    {
                        valMin = 0;
                        valMax = 0;
                    }
                    else
                    {
                        valMin = Math.Max(valMin, Value);
                    }
                    break;
                case Operation.Greater:
                    if (valMin > Value)
                    {
                        valMin = 0;
                        valMax = 0;
                    }
                    else
                    {
                        valMax = Math.Min(valMax, Value);
                    }
                    break;
                default: throw new UnreachableException();
            }

            return Cat switch
            {
                Category.X => (min with { X = valMin }, max with { X = valMax }),
                Category.M => (min with { M = valMin }, max with { M = valMax }),
                Category.A => (min with { A = valMin }, max with { A = valMax }),
                Category.S => (min with { S = valMin }, max with { S = valMax }),
                _ => throw new UnreachableException(),
            };
        }
    }

    public (object, object) Solve(string input)
    {
        var pars = Utils.SplitPars(input);

        var workflows = (
            from line in Utils.SplitLines(pars[0])
            let firstBrace = line.IndexOf('{')
            let name = line[0..firstBrace]
            let rules = (
                from rule in Utils.SplitAtCommas(line[(firstBrace + 1)..^1])
                select new Rule(rule)
            ).ToArray()
            select (name, rules)
        ).ToDictionary(x => x.name, x => x.rules);

        // Some workflows only contains rules that reject or accept the part
        // with no other destinations.
        // We simplify them by making them contain only a single A/R rule.
        //
        // (Actually, this optimisation doesn't really change the running time,
        // so now I'm wondering if there's a reason the input has this kind of workflows.)
        foreach (var item in workflows)
        {
            if (item.Value.All(r => r.Dest == "A"))
            {
                workflows[item.Key] = new[] { new Rule(Operation.True, -1, Category.X, "A") };
            }

            if (item.Value.All(r => r.Dest == "R"))
            {
                workflows[item.Key] = new[] { new Rule(Operation.True, -1, Category.X, "R") };
            }
        }

        var parts = (
            from line in Utils.SplitLines(pars[1])
            select new Part(line)
        ).ToArray();

        return (Answer1(workflows, parts), Answer2(workflows));
    }

    static long Answer1(Dictionary<string, Rule[]> workflows, Part[] parts)
    {
        long sum = 0;
        foreach (var part in parts)
        {
            var name = "in";
            while (name != "A" && name != "R")
            {
                foreach (var rule in workflows[name])
                {
                    var result = rule.Follow(part);
                    if (result is not null)
                    {
                        // The input's workflows always have a succeeding rule.
                        name = result;
                        break;
                    }
                }
            }

            if (name == "A")
            {
                sum += part.X + part.M + part.A + part.S;
            }
        }
        return sum;
    }

    // This class goes from a succeeding rule
    // and back in time to the "in" rule,
    // saving the range of parts that allows reaching the succeeding rule.
    class TimeMachine
    {
        Dictionary<string, Rule[]> Workflows { get; }

        // Maps a workflow name
        // to all the workflow names
        // and the index of their rule leading to the former workflow.
        // Hope it's clear.
        Dictionary<string, List<(string, int)>> LeadingTo { get; }

        public List<(Part, Part)> Ranges { get; }

        public TimeMachine(Dictionary<string, Rule[]> workflows)
        {
            Workflows = workflows;
            Ranges = new();
            LeadingTo = new();

            foreach (var w in workflows)
            {
                for (int i = 0; i < w.Value.Length; i++)
                {
                    var dest = w.Value[i].Dest;
                    if (!LeadingTo.ContainsKey(dest))
                    {
                        LeadingTo.Add(dest, new());
                    }

                    LeadingTo[dest].Add((w.Key, i));
                }
            }

            // Go back in time for all the rules that accept parts.
            foreach (var (name, i) in LeadingTo["A"])
            {
                var minPart = new Part(1, 1, 1, 1);
                var maxPart = new Part(4000, 4000, 4000, 4000);
                GoBackWorkflow(name, i, minPart, maxPart);
            }
        }

        void GoBackWorkflow(string name, int i, Part min, Part max)
        {

            // The current rule must succeed.
            (min, max) = Workflows[name][i].MakesSucceed(min, max);

            for (int j = i - 1; j >= 0; j--)
            {
                // The previous rules must fail.
                (min, max) = Workflows[name][j].MakesFail(min, max);
            }

            if (LeadingTo.TryGetValue(name, out var next))
            {
                foreach (var (nameNext, iNext) in next)
                {
                    GoBackWorkflow(nameNext, iNext, min, max);
                }
            }
            else
            {
                Ranges.Add((min, max));
            }
        }
    }


    static long Answer2(Dictionary<string, Rule[]> workflows)
    {
        var timeMachine = new TimeMachine(workflows);

        // Fortunately, the ranges don't overlap!
        // Otherwise, we would have needed to compute intersection of hypercubes...
        // (Because there are 4 values.)
        return (
            from range in timeMachine.Ranges
            let min = range.Item1
            let max = range.Item2
            where min.X != 0 && min.M != 0 && min.A != 0 && min.S != 0
            // + 1 because the ranges are inclusive.
            select (max.X - min.X + 1) * (max.M - min.M + 1) * (max.A - min.A + 1) * (max.S - min.S + 1)
        ).Sum();
    }
}
