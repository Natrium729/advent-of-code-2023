using System.Diagnostics;

namespace Natrium729.AdventOfCode2023;

public class Day12 : IDay
{
    class Row
    {
        public char[] Springs { get; }
        public int[] Counts { get; }

        public Row(string line, bool unfold = false)
        {
            var words = Utils.SplitWords(line);
            if (!unfold)
            {
                Springs = words[0].ToArray();
                Counts = (
                    from count in words[1].Split(",")
                    select int.Parse(count)
                ).ToArray();
            }
            else
            {
                Springs = string.Join("?", Enumerable.Repeat(words[0], 5)).ToArray();
                Counts = (
                    from count in string.Join(",", Enumerable.Repeat(words[1], 5)).Split(",")
                    select int.Parse(count)
                ).ToArray();
            }
        }

        // That is the non-memoised way that was used to solve part 1,
        // but is no longer used in favour of the memoised one.
        IEnumerable<string> Arrangements()
        {
            var unknowns = (
                from x in Enumerable.Range(0, Springs.Length).Zip(Springs)
                where x.Second == '?'
                select x.First
            ).ToArray();
            var nUnknowns = unknowns.Length;
            var arrangement = new char[Springs.Length];
            Springs.CopyTo(arrangement, 0);

            for (uint mask = 0; mask < Math.Pow(2, nUnknowns); mask++)
            {
                for (int i = 0; i < nUnknowns; i++)
                {
                    var state = (mask >> i) & 1;
                    if (state == 1)
                    {
                        arrangement[unknowns[i]] = '#';
                    }
                    else
                    {
                        arrangement[unknowns[i]] = '.';
                    }
                }

                yield return string.Concat(arrangement);
            }
        }

        // That is the non-memoised way that was used to solve part 1,
        // but is no longer used in favour of the memoised one.
        public long CountArrangements()
        {
            return (
                from arrangement in Arrangements()
                let groups = arrangement.Split('.', StringSplitOptions.RemoveEmptyEntries)
                where groups.Length == Counts.Length
                where groups.Zip(Counts).All(x => x.First.Length == x.Second)
                select arrangement
            ).LongCount();
        }
    }

    class Checker
    {
        Row Row { get; }
        Dictionary<(int, int, int), long> Memorised { get; } = new();

        public Checker(Row row)
        {
            Row = row;
        }

        // In the functions  below, the string argument `soFar`
        // was used for debugging.
        // I guess it add a bit of useless overhead but it's OK.

        public long CountArrangements()
        {
            return Next(-1, 0, 0, "");
        }

        long Next(int i, int iCounts, int damagedSoFar, string soFar)
        {
            // We are checking the next spring.
            i++;

            // We got to the end of the row.
            if (i == Row.Springs.Length)
            {
                // We don't have a last group we didn't check yet.
                // The arrangement is valid if we counted all the groups.
                if (damagedSoFar == 0)
                {
                    if (iCounts == Row.Counts.Length)
                    {
                        // Console.WriteLine($"{soFar}, {1}");
                        return 1;
                    }

                    return 0;
                }

                // We check the last group is for the last count
                // and that the count is good.
                if (
                    iCounts == Row.Counts.Length - 1
                    && damagedSoFar == Row.Counts[iCounts])
                {
                    // Console.WriteLine($"{soFar}, {1}");
                    return 1;
                }

                // Console.WriteLine($"{soFar}, {0}");
                return 0;
            }

            if (Memorised.TryGetValue((i, iCounts, damagedSoFar), out long val))
            {
                return val;
            }

            switch (Row.Springs[i])
            {
                case '#':
                    var result = Damaged(i, iCounts, damagedSoFar, soFar);
                    Memorised.Add((i, iCounts, damagedSoFar), result);
                    return result;
                case '.':
                    var result2 = Operational(i, iCounts, damagedSoFar, soFar);
                    Memorised.Add((i, iCounts, damagedSoFar), result2);
                    return result2;
                case '?':
                    var result3 = Damaged(i, iCounts, damagedSoFar, soFar) + Operational(i, iCounts, damagedSoFar, soFar);
                    Memorised.Add((i, iCounts, damagedSoFar), result3);
                    // We fork here, and try both possibilities.
                    return result3;
                default: throw new UnreachableException();
            };
        }

        long Damaged(int i, int iCounts, int damagedSoFar, string soFar)
        {
            soFar += '#';

            // We encountered a damaged spring.
            damagedSoFar++;

            // But if there are no groups left anyway,
            // the arrangement can't be valid.
            if (iCounts == Row.Counts.Length)
            {
                // Console.WriteLine($"{soFar}, {0}");
                return 0;
            }

            // If the damaged springs we encountered is greater that the current count,
            // We can stop right now, the arrangement is not valid.
            if (damagedSoFar > Row.Counts[iCounts])
            {
                // Console.WriteLine($"{soFar}, {0}");
                return 0;
            }

            return Next(i, iCounts, damagedSoFar, soFar);
        }

        long Operational(int i, int iCounts, int damagedSoFar, string soFar)
        {
            soFar += ".";

            // It's the end of a group.
            if (damagedSoFar != 0)
            {
                // If there are too many groups of damaged springs,
                // then the arrangement can't be valid.
                if (iCounts == Row.Counts.Length)
                {
                    // Console.WriteLine($"{soFar}, {0}");
                    return 0;
                }

                // If the group size is not the same as the current count,
                // the arrangement is not valid.
                if (damagedSoFar != Row.Counts[iCounts])
                {
                    // Console.WriteLine($"{soFar}, {0}");
                    return 0;
                }

                // We are now on the next group.
                iCounts++;
            }

            // We reset the damaged so far to 0.
            return Next(i, iCounts, 0, soFar);
        }
    }

    public (object, object) Solve(string input)
    {
        return (Answer1(input), Answer2(input));
    }

    static long Answer1(string input)
    {
        var rows = (
            from line in Utils.SplitLines(input)
            select new Row(line)
        ).ToArray();

        return (
            from row in rows
            select new Checker(row).CountArrangements()
        ).Sum();


        // That was the old iterative way without memoisation.

        // return (
        //     from row in rows
        //     select row.CountArrangements()
        // ).Sum();
    }

    static long Answer2(string input)
    {
        var rows = (
            from line in Utils.SplitLines(input)
            select new Row(line, unfold: true)
        ).ToArray();

        return (
            from row in rows
            select new Checker(row).CountArrangements()
        ).Sum();
    }
}
