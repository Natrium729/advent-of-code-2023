namespace Natrium729.AdventOfCode2023;

public class Day15 : IDay
{
    static int Hash(string str)
    {
        var result = 0;

        foreach (var ch in str)
        {
            result += ch;
            result *= 17;
            result %= 256;
        }

        return result;
    }

    readonly record struct Lens
    {
        public string Label { get; init; }
        public int Focal { get; init; }
    }

    public (object, object) Solve(string input)
    {
        var steps = Utils.SplitAtCommas(input);
        return (Answer1(steps), Answer2(steps));
    }

    static long Answer1(string[] steps)
    {
        return steps.Select(step => (long)Hash(step)).Sum();
    }

    static long Answer2(string[] steps)
    {
        var boxes = new List<Lens>[256];
        for (int i = 0; i < boxes.Length; i++)
        {
            boxes[i] = new();
        }

        foreach (var step in steps)
        {
            if (step.EndsWith('-'))
            {
                var label = step[..(step.Length - 1)];
                var box = boxes[Hash(label)];
                var iLens = box.FindIndex(lens => lens.Label == label);
                if (iLens >= 0)
                {
                    box.RemoveAt(iLens);
                }
            }
            else
            {
                var split = step.Split('=');
                var label = split[0];
                var focal = int.Parse(split[1]);
                var lens = new Lens { Label = label, Focal = focal };

                var box = boxes[Hash(label)];
                var iLens = box.FindIndex(lens => lens.Label == label);
                if (iLens >= 0)
                {
                    box[iLens] = new Lens { Label = label, Focal = focal };
                }
                else
                {
                    box.Add(lens);
                }
            }
        }

        return (
            from a in boxes.Select((box, i) => (box, i))
            let box = a.box
            let iBox = a.i
            from b in box.Select((lens, i) => (lens, i))
            let lens = b.lens
            let iLens = b.i
            select (1 + iBox) * (1 + iLens) * lens.Focal
        ).Sum();
    }
}
