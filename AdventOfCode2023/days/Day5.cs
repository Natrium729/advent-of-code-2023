namespace Natrium729.AdventOfCode2023;

public class Day5 : IDay
{
    readonly record struct Range
    {
        public long SourceStart { get; init; }

        public long DestinationStart { get; init; }

        public long Length { get; init; }

        public long? SourceToDest(long source)
        {
            if (SourceStart <= source && source < SourceStart + Length)
            {
                return DestinationStart + source - SourceStart;
            }

            return null;
        }
    }

    class Mapping
    {
        public string DestName { get; }
        Range[] Ranges { get; }

        public Mapping(string destName, Range[] ranges)
        {
            DestName = destName;
            Array.Sort(ranges, (a, b) => (int)(a.SourceStart - b.SourceStart));
            Ranges = ranges;
        }

        public long SourceToDest(long source)
        {
            foreach (var range in Ranges)
            {
                if (range.SourceToDest(source) is long dest)
                {
                    return dest;
                }
            }

            return source;
        }


        // Part 2, attempt 1.
        //
        // We advance start until the next important point
        // (the start of the next range or the end of the current range,
        // depending of whether start is inside a range or not)
        // and map the range we just advanced.
        //
        // Works with the example, fails with the real input.
        /*
        public (long, long)[] SourceToDest(long start, long length)
        {
            var dests = new List<(long, long)>();
            var end = start + length;
            int i = 0;
            while (i < Ranges.Length && Ranges[i].SourceStart + Ranges[i].Length <= start)
            {
                i++;
            }
            while (start < end)
            {
                if (i >= Ranges.Length) // Start past last range.
                {
                    dests.Add((start, end - start));
                    start = end;
                }
                else if (start < Ranges[i].SourceStart) // Start before current range
                {
                    var rangeEnd = Math.Min(Ranges[i].SourceStart, end);
                    dests.Add((start, rangeEnd - start));
                    start = rangeEnd;
                }
                else // Start inside current range
                {
                    var range = Ranges[i];
                    long destStart = (long)range.SourceToDest(start)!;
                    var rangeEnd = Math.Min(range.SourceStart + range.Length, end);
                    dests.Add((destStart, rangeEnd - start));
                    start = rangeEnd;
                    i++;
                }
            }
            return dests.ToArray();
        }
        */

        // Part 2, attempt 2
        //
        // We loop through the ranges
        // and map the part of the source that comes before its start,
        // and map the part of the source that is inside.
        //
        // Works with the example, fails with the real input.
        // (I believe it's equivalent to attempt 1.)
        /*
        public (long, long)[] SourceToDest(long start, long length)
        {
            var dests = new List<(long, long)>();
            long end = start + length;
            foreach (var range in Ranges)
            {
                if (range.SourceStart + range.Length <= start)
                {
                    continue;
                }

                if (start < range.SourceStart)
                {
                    var rangeEnd = Math.Min(range.SourceStart, end);
                    dests.Add((start, rangeEnd - start));
                    start = rangeEnd;

                    if (start == end)
                    {
                        break;
                    }
                }

                if (range.SourceStart <= start && start < range.SourceStart + range.Length)
                {
                    var rangeEnd = Math.Min(range.SourceStart + range.Length, end);
                    var dest = range.DestinationStart + start - range.SourceStart;
                    dests.Add((dest, rangeEnd - start));
                    start = rangeEnd;

                    if (start == end)
                    {
                        break;
                    }
                }
            }

            if (start < end)
            {
                dests.Add((start, end - start));
            }

            return dests.ToArray();
        }
        */

        // Part 2, attempt 3.
        //
        // We keep a stack of pending ranges we haven't processed yet.
        //
        // As long as there is a pending range,
        // we try to find a mapping range it intersects.
        // If we find one, we map the intersection
        // and push the parts that don't intersect to the pending stack.
        //
        // If the pending range didn't intersect with any of the mapping ranges,
        // then they are mapped to the same values.
        //
        // And... it works! Yay!
        //
        // (My theory: there's an off by one error somewhere
        // in each of the two first attempts.
        // Because I'm quite sure the idea behind them works.)
        public (long, long)[] SourceToDest(long start, long length)
        {
            // During the algorithm, we'll keep track of the ranges
            // as (start, end), inclusive.
            // It seems easier not to make an off-by-one error.
            var pending = new Stack<(long, long)>();
            pending.Push((start, start + length - 1));

            // But the destinations will be in the form (start, length)
            // to keep the way the problem is presented.
            var destinations = new List<(long, long)>();

            while (pending.TryPop(out var sourceRange))
            {
                // Will keep track whether we found a mapping range that intersects.
                var overlapped = false;

                var (sourceStart, sourceEnd) = sourceRange;

                // It works because we know the mapping range don't intersect.
                // (Otherwise the answer would be ambiguous.)
                foreach (var range in Ranges)
                {
                    var (currentStart, currentEnd) = (range.SourceStart, range.SourceStart + range.Length - 1);

                    // If the source doesn't intersect, continue to the next mapping range.
                    if (sourceStart > currentEnd || sourceEnd < currentStart)
                    {
                        continue;
                    }

                    // If we are here, there's an intersection!
                    // So we map the intersection to the destination according to the mapping range.
                    var destStart = range.DestinationStart + Math.Max(sourceStart, currentStart) - range.SourceStart;
                    var destEnd = range.DestinationStart + Math.Min(sourceEnd, currentEnd) - range.SourceStart;
                    destinations.Add((destStart, destEnd - destStart + 1));

                    // If there's a part that doesn't intersect before,
                    // push it to the pending stack.
                    if (sourceStart < currentStart)
                    {
                        pending.Push((sourceStart, currentStart - 1));
                    }

                    // If there's a part that doesn't intersect after,
                    // push it to the pending stack.
                    if (sourceEnd > currentEnd)
                    {
                        pending.Push((currentEnd + 1, sourceEnd));
                    }

                    // We found an intersection, stop searching.
                    overlapped = true;
                    break;
                }

                // If the source didn't intersect any mapping range,
                // then map it to itself.
                if (!overlapped)
                {
                    destinations.Add((sourceStart, sourceEnd - sourceStart + 1));
                }
            }

            return destinations.ToArray();
        }
    }


    public (object, object) Solve(string input)
    {
        var paragraphs = input.Split("\n\n", StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries);

        var seeds = (
            from word in paragraphs[0].Split(" ", StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries).Skip(1)
            select long.Parse(word)
        ).ToArray();

        var mappings = new Dictionary<string, Mapping>();
        foreach (var paragraph in paragraphs.Skip(1))
        {
            var lines = paragraph.Split("\n", StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries);

            // "seed-to-soil map:" -> { "seed", "soil" }
            var sourceDest = lines[0].Split(" ")[0].Split("-to-");

            var ranges = (
                from line in lines.Skip(1)
                let numbers = (
                    from word in line.Split(" ", StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries)
                    select long.Parse(word)
                ).ToArray()
                select new Range { SourceStart = numbers[1], DestinationStart = numbers[0], Length = numbers[2] }
            ).ToArray();

            mappings[sourceDest[0]] = new Mapping(sourceDest[1], ranges);
        }

        return (Answer1(seeds, mappings), Answer2(seeds, mappings));
    }


    static long Answer1(long[] seeds, Dictionary<string, Mapping> mappings)
    {

        return seeds.Select(seed =>
        {
            var category = "seed";
            var source = seed;
            while (mappings.TryGetValue(category, out Mapping? currentMapping))
            {
                category = currentMapping.DestName;
                source = currentMapping.SourceToDest(source);
            }
            return source;
        }).Min();
    }

    static long Answer2(long[] seeds, Dictionary<string, Mapping> mappings)
    {
        var sources = from range in seeds.Chunk(2) select (range[0], range[1]);
        var category = "seed";
        while (mappings.TryGetValue(category, out Mapping? currentMapping))
        {
            category = currentMapping.DestName;
            var dests = new List<(long, long)>();
            foreach (var (start, len) in sources)
            {
                dests.AddRange(currentMapping.SourceToDest(start, len));
            }
            sources = dests.ToArray();
        }

        return sources.MinBy(x => x.Item1).Item1;
    }
}
