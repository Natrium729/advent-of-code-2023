using System.Diagnostics;

namespace Natrium729.AdventOfCode2023;

public class Day16 : IDay
{
    public record struct Beam
    {
        public int Line;
        public int Col;
        public Direction Dir;

        public Beam(int line, int col, Direction dir)
        {
            Line = line;
            Col = col;
            Dir = dir;
        }
    }

    class Contraption
    {
        public char[][] Tiles { get; }

        public Contraption(string input)
        {
            Tiles = Utils.SplitLines(input).Select(line => line.ToArray()).ToArray();
        }


        public long CountEnergizedTiles(Beam start)
        {
            // The tiles we visited.
            // We take account of the direction we visited them to track loops.
            var energized = new HashSet<Beam>();

            // The end of each beams.
            var beams = new HashSet<Beam>() { start };

            // The count of energized tiles of the previous iterations.
            var oldCount = 0;

            // We loop until the current count of energized tiles
            // is the same as in the previous interation
            do
            {
                oldCount = energized.Count;
                var newBeams = new HashSet<Beam>();
                foreach (var beam in beams)
                {
                    energized.Add(beam);
                    var line = beam.Line;
                    var col = beam.Col;
                    var dir = beam.Dir;
                    switch ((Tiles[line][col], dir))
                    {
                        // Empty space and pointy end of splitter.
                        case ('.', Direction.Up):
                        case ('|', Direction.Up):
                            newBeams.Add(new(line - 1, col, dir));
                            break;
                        case ('.', Direction.Down):
                        case ('|', Direction.Down):
                            newBeams.Add(new(line + 1, col, dir));
                            break;
                        case ('.', Direction.Left):
                        case ('-', Direction.Left):
                            newBeams.Add(new(line, col - 1, dir));
                            break;
                        case ('.', Direction.Right):
                        case ('-', Direction.Right):
                            newBeams.Add(new(line, col + 1, dir));
                            break;

                        // Mirror.
                        case ('/', Direction.Up):
                            newBeams.Add(new(line, col + 1, Direction.Right));
                            break;
                        case ('/', Direction.Down):
                            newBeams.Add(new(line, col - 1, Direction.Left));
                            break;
                        case ('/', Direction.Left):
                            newBeams.Add(new(line + 1, col, Direction.Down));
                            break;
                        case ('/', Direction.Right):
                            newBeams.Add(new(line - 1, col, Direction.Up));
                            break;

                        // Back-mirror.
                        case ('\\', Direction.Up):
                            newBeams.Add(new(line, col - 1, Direction.Left));
                            break;
                        case ('\\', Direction.Down):
                            newBeams.Add(new(line, col + 1, Direction.Right));
                            break;
                        case ('\\', Direction.Left):
                            newBeams.Add(new(line - 1, col, Direction.Up));
                            break;
                        case ('\\', Direction.Right):
                            newBeams.Add(new(line + 1, col, Direction.Down));
                            break;

                        // Flat side of splitter
                        case ('|', Direction.Left):
                        case ('|', Direction.Right):
                            newBeams.Add(new(line - 1, col, Direction.Up));
                            newBeams.Add(new(line + 1, col, Direction.Down));
                            break;
                        case ('-', Direction.Up):
                        case ('-', Direction.Down):
                            newBeams.Add(new(line, col - 1, Direction.Left));
                            newBeams.Add(new(line, col + 1, Direction.Right));
                            break;

                        default: throw new UnreachableException();
                    }
                }

                // Updates the beams.
                beams.Clear();
                var validBeams =
                    from b in newBeams
                        // Remove the beams that left the contraption.
                    where 0 <= b.Line && b.Line < Tiles.Length
                    where 0 <= b.Col && b.Col < Tiles[0].Length
                    // Remove the beams that are looping.
                    where !energized.Contains(b)
                    select b;
                beams.UnionWith(validBeams);
            } while (oldCount != energized.Count);

            // Count the energized beams
            // without taking account of the directions.
            return (
                from tile in energized
                select (tile.Line, tile.Col)
            ).Distinct().Count();
        }
    }

    public enum Direction
    {
        Up,
        Right,
        Down,
        Left,
    }


    public (object, object) Solve(string input)
    {
        var contraption = new Contraption(input);
        return (Answer1(contraption), Answer2(contraption));
    }

    static long Answer1(Contraption contraption)
    {
        return contraption.CountEnergizedTiles(new(0, 0, Direction.Right));
    }

    static long Answer2(Contraption contraption)
    {
        long count = 0;

        for (int line = 0; line < contraption.Tiles.Length; line++)
        {
            count = Math.Max(count, contraption.CountEnergizedTiles(new(line, 0, Direction.Right)));
            count = Math.Max(count, contraption.CountEnergizedTiles(new(line, contraption.Tiles[line].Length - 1, Direction.Left)));
        }

        for (int col = 0; col < contraption.Tiles[0].Length; col++)
        {
            count = Math.Max(count, contraption.CountEnergizedTiles(new(0, col, Direction.Down)));
            count = Math.Max(count, contraption.CountEnergizedTiles(new(contraption.Tiles.Length - 1, col, Direction.Up)));
        }

        return count;
    }
}
