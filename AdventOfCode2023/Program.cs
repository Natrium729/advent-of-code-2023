﻿using Natrium729.AdventOfCode2023;

// It would have been better if the `Solve` method was static,
// because it doesn't need state and then we wouldn't need useless instantiations,
// but I couldn't find a way to have an array of types implementing `IDay`
// (instead of an array of instances).
//
// I guess we could have an array of delegates?
// But at that point, you know, just give me free functions… :'(
// (Maybe I just hate object-oriented programming.)
//
// Scratch that, actually some days do have state.
// Sometimes, the example and the actual puzzles have different parameters,
// so we keep them in properties of days.

if (args.Length > 0)
{
    foreach (var arg in args)
    {
        if (int.TryParse(arg, out int parsed))
        {
            if (parsed < 1 || parsed > 25)
            {
                Console.WriteLine($"The day number {parsed} is not between 1 and 25 inclusive.");
            }
            else
            {
                Solve(parsed);
            }
        }
        else
        {
            Console.WriteLine($"Invalid day number: {arg}");
        }
    }
}
else
{
    for (int i = 1; i <= 25; i++)
    {
        Solve(i);
    }
}

IDay? GetDay(int day)
{
    return day switch
    {
        1 => new Day1(),
        2 => new Day2(),
        3 => new Day3(),
        4 => new Day4(),
        5 => new Day5(),
        6 => new Day6(),
        7 => new Day7(),
        8 => new Day8(),
        9 => new Day9(),
        10 => new Day10(),
        11 => new Day11(),
        12 => new Day12(),
        13 => new Day13(),
        14 => new Day14(),
        15 => new Day15(),
        16 => new Day16(),
        17 => new Day17(),
        18 => new Day18(),
        19 => new Day19(),
        20 => new Day20(),
        21 => new Day21(),
        22 => new Day22(),
        _ => null
    };
}

void Solve(int dayNum)
{
    var day = GetDay(dayNum);
    var (answer1, answer2) = day switch
    {
        null => (null, null),
        _ => day.Solve(File.ReadAllText($"inputs/day{dayNum}.txt"))
    };

    Console.Write($"Day {dayNum}.1: ");
    if (answer1 == null)
    {
        Console.WriteLine("unsolved");
    }
    else
    {
        Console.WriteLine(answer1);
    }

    Console.Write($"Day {dayNum}.2: ");
    if (answer2 == null)
    {
        Console.WriteLine("unsolved");
    }
    else
    {
        Console.WriteLine(answer2);

    }
}

interface IDay
{
    abstract (object, object) Solve(string input);
}
