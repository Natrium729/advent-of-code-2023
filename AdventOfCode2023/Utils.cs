﻿using Natrium729.AdventOfCode2023;

public class Utils
{
    public static string[] SplitLines(string input)
    {
        string[] sep = { "\r\n", "\n" };
        return input.Split(sep, StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries);
    }

    public static string[] SplitWords(string input)
    {
        return input.Split(" ", StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries);
    }

    public static string[] SplitAtCommas(string input)
    {
        return input.Split(",", StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries);
    }

    public static string[] SplitPars(string input)
    {
        string[] sep = { "\r\n\r\n", "\n\n" };
        return input.Split(sep, StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries);
    }

    // It seems there's no equivalent for `Enumable.Range` with longs,
    // so we have to do it ourselves. :(
    public static IEnumerable<long> RangeLong(long start, long count)
    {
        for (long i = start; i < start + count; i++)
        {
            yield return i;
        }
    }

    public static long Gcd(long a, long b)
    {
        if (b == 0) {
            return a;
        }

        return Gcd(b, a % b);
    }

    public static long Lcm(long a, long b)
    {
        return Math.Abs(a * b) / Gcd(a, b);
    }
}
